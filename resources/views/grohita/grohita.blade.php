@extends('layouts.master')
@section('content')

<div class="container-fluid">
    <div class="row-fluid">
      <div class="span2"> </div>     
        <div class="span8">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <a href="{{url('grohita/create')}}" class="btn btn-primary">+গ্রহিতা সংযোজন করুন</a>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                   <th>গ্রহিতার নাম</th>      
                   <th>ক্রিয়া সমূহ</th>
                </tr>
              </thead>
              <tbody>

          		@foreach($grohitas as $grohi)
                <tr class="gradeX">
                  
                    <td class="center">{{ $grohi-> grohitar_name}}</td>
                    
                    <td>
              <a href='grohita/edit/{{$grohi->id}}' class='btn btn-xs btn-success'><i class='fa fa-edit'>পুনরায় ঠিক করুন</i></a>
              <a href='grohita/delete/{{$grohi->id}}' class='btn btn-xs btn-danger'><i class='fa fa-trash-o' onClick='return doconfirm();'>মুছে ফেলুন</i></a>
            </td>
                </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
        </div>       
      </div>
           <div class="span2"></div>
         </div>
       </div>
   
<script>
  function doconfirm()
    {
        job=confirm("আপনি কি নিশ্চিত যে আপনি এই রেকর্ড মুছে দিতে চান?");
        if(job!=true)
        {
            return false;
        }
    }
</script>
@endsection