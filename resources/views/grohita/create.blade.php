@extends('layouts.master')
@section('content')
 <div class="row-fluid">
      <div class="span3"></div>
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>গ্রহিতা সংযোজন</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal"  action="{{url('grohita/create')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
              <div class="control-group">
                <label class="control-label">গ্রহিতার নাম</label>
                <div class="controls">
                  <input type="text"  placeholder="গ্রহিতার নাম" class="span11" name="grohitar_name" required="required">
                 </div>
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
                
              </div>
              </form>
          </div>
        </div>
      </div>

    <div class="span3"></div>

    </div>
@endsection