@extends('layouts.master')
@section('content')
<div id="content">
  <div id="content-header"></div>
  <div class="container-fluid">
  
    <div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>দাগ রেজিস্টার</h5>
          </div>
          <div class="widget-content nopadding"><br>
           <form class="form-horizontal"  action="{{url('dagrejister/update')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                  <input type="hidden" name="txtId" value="{{$row->id}}" class="form-control" readonly /> 
                  <div class="form-group">
                    <label class="control-label">সাফ কবলা ফাইল নং</label>
                       <div class="col-sm-3">
                          <input type="text" placeholder="সাফ কবলা ফাইল নং" class="span11" value="{{$row->saf_kobola_number}}" name="saf_kobola_number" required="required">
                      </div>
                      <label class="control-label">দাতা/দাত্রীর নাম</label>
                  <div class="col-sm-3">
                  <input type="text"  placeholder="দাতা/দাত্রীর নাম" class="span11" value="{{$row->data_or_datrir_name}}" name="data_or_datrir_name" required="required">
                </div>
                </div>
              
              
              <div class="form-group">
                <label class="control-label">গ্রহীতার নাম</label>
                   <div class="col-sm-3">
                 <?php
                  $all_name = DB::table('grohita')->get();
                ?>
                  <select name="grohitar_name"  required="required" class="span11" >
               
                      @foreach ($all_name as $v_name)
                      <option value="{{$v_name->grohitar_name}}" <?php if ($row->grohitar_name == $v_name->grohitar_name) echo ' selected="selected"';?>><?php echo $v_name->grohitar_name ?></option>
                      
                      
                      @endforeach
                  </select>
                 </div>
                  <label class="control-label">মিডিয়ার নাম</label>
              <div class="col-sm-3">
                 <input type="text" placeholder="মিডিয়ার নাম" value="{{$row->name_of_media}}"  class="span11" name="name_of_media" required="required" >
                </div>
              </div>
              
               <div class="form-group">
                <label class="control-label">দলিল নং</label>
              <div class="col-sm-3">
                 <input type="text" placeholder="দলিল নং" class="span11" value="{{$row->dolil_number}}"  name="dolil_number" required="required">
                </div>
                <label class="control-label">তারিখ</label>
                 <div class="col-sm-3">
                  <input type="text"  class="span11"  name="date" value="{{$row->date}}" >
                </div>
               
              </div>
              
             
              
               <div class="form-group">
                <label class="control-label">মৌজার নাম</label>
            <div class="col-sm-3">
                 <?php
                  $all_name = DB::table('mouja')->get();
                ?>
                  <select name="mouja" id="mouja" required="required" class="span11" >
               
                      @foreach ($all_name as $v_name) 
                      <option value="{{$v_name->id}}"  <?php if ($row->mouja_id == $v_name->id) echo ' selected="selected"';?>><?php echo $v_name->nameOfMouja ?></option>
                      @endforeach
                  </select>
                 </div>
              <label class="control-label">আর এস দাগ নং</label>
                <div class="col-sm-3">
                  <?php
                  $all_dag = DB::table('mouja_dag')->where('mouja_id',$row->mouja_id)->get();
                ?>
                 <select id="rs_dag_no" class="span11" name="rs_dag_no">
                    <option value="">আর এস দাগ নং</option>
                    @foreach ($all_dag as $dag) 
                      <option value="{{$dag->rs_dag_no}}"  <?php if ($row->rs_dag_no == $dag->rs_dag_no) echo ' selected="selected"';?>><?php echo $dag->rs_dag_no ?></option>
                       
                       
                      @endforeach
                  </select>
                </div>
                
              </div>
             
              <div class="form-group">
              <label class="control-label">এস এ দাগ নং</label>
                  <div class="col-sm-3">
                  <input type="text" placeholder="এস এ দাগ নং" class="span11" name="sadag_no" value="{{$row->sadag_no}}" required="required" readonly>
                </div>
                <label class="control-label">আর এস দাগে মোট পরিমাণ (শতাংশ)</label>
               <div class="col-sm-3">
                   <input type="text" placeholder="আর এস দাগে মোট পরিমাণ" class="span11" value="{{$row->rs_dag_a_poriman}}" name="rs_dag_a_poriman" required="required" readonly>
                </div>
              </div>
               <div class="form-group">
                <label class="control-label">খাস জমি (শতাংশ)</label>
               <div class="col-sm-3">
                   <input type="text" placeholder="খাস জমি" class="span11" value="{{$row->kas_jomi}}" name="kas_jomi" required="required" readonly>
                </div>
                <label class="control-label">অধিগ্রহণ কৃত জমি(শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="অধিগ্রহণ কৃত জমি" class="span11" value="{{$row->odigrohan_krrito_jomi}}" name="odigrohan_krrito_jomi" required="required" readonly>
                </div>
              </div>
           
                
            
              <div class="form-group">
                <label class="control-label">R/S দাগে মোট ক্রয়যোগ্য<br>জমি (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="R/S দাগে মোট ক্রয়যোগ্য জমি" class="span11" value="{{$row->rs_dagay_total_jomi}}" name="rs_dagay_total_jomi" required="required" readonly>
                </div>
              </div>


              
              <div class="form-group">
                <label class="control-label">R/S দাগে পূর্বে মোট<br>ক্রয়কৃত জমি (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="ক্রয়কৃত জমির পরিমাণ" class="span11" id="totalRegister" value="{{$row->totalRegister}}" readonly="readonly">
                </div>

                <label class="control-label">R/S সর্বমোট ক্রয়যোগ্য<br>জমির পরিমাণ (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="ক্রয়কৃত জমির পরিমাণ" class="span11" name="rs_remaing_land" id="rs_remaing_land" value="{{ ($row->rs_dagay_total_jomi-$row->totalRegister) }}" required="required" readonly="readonly">
                </div>
              </div> 
                
            
              <div class="form-group">
                <label class="control-label">ক্রয়কৃত জমির পরিমাণ (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="ক্রয়কৃত জমির পরিমাণ" class="span11" value="{{$row->total_purchased_land}}" name="total_purchased_land"  id="total_purchased_land" required="required" onchange="landValidation()">
                </div>
              </div>

            <div class="form-group">
                <label class="control-label">বায়নাকৃত জমির পরিমাণ (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="বায়নাকৃত জমির পরিমাণ" class="span11" value="{{$row->baynakrito_jomi}}" name="baynakrito_jomi" required="required">
                </div>
                <label class="control-label">অবশিষ্ট জমির পরিমাণ (শতাংশ)</label>
               <div class="col-sm-3">
                   <input type="text" placeholder="অবশিষ্ট জমির পরিমাণ" class="span11" value="{{$row->remaining_land}}" name="remaining_land" id="remaining_land" required="required" readonly>
                </div>
              </div>
         
                
            
        <div class="form-group">
                <label class="control-label">সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি ((শতাংশ)</label>
     <div class="col-sm-3">
                   <input type="text" placeholder="সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি" class="span11" value="{{$row->smonnoy_extra_purchased_land}}" name="smonnoy_extra_purchased_land" required="required">
                </div>
                <label class="control-label">নামজারীকৃত জমি (শতাংশ)</label>
           <div class="col-sm-3">
                   <input type="text" placeholder="নামজারীকৃত জমি" class="span11" value="{{$row->namjari_krito_land}}" name="namjari_krito_land" id="namjari_krito_land" required="required" onchange="namjari()">
                </div>
              </div>
               
                
             
               <div class="form-group">
                <label class="control-label">নামজারী বিহীন জমি<br>(শতাংশ)</label>
            <div class="col-sm-3">
                   <input type="text" placeholder="নামজারী বিহীন জমি" class="span11" value="{{$row->namjari_bihin_land}}" name="namjari_bihin_land" id="namjari_bihin_land" required="required" readonly>
                </div>
                <label class="control-label">জোত নাম্বার</label>
   <div class="col-sm-3">
                   <input type="text" placeholder="জোত নাম্বার" class="span11" value="{{$row->jooth_number}}" name="jooth_number" required="required">
                </div>
              </div>
               <div class="form-group">
                <label class="control-label">নামজারী কেইস নাম্বার  </label>
            <div class="col-sm-3">
                   <input type="text" placeholder="নামজারী কেইস নাম্বার" class="span11" value="{{$row->keis_no}}" name="keis_no" id="keis_no" required="required">
                </div>
                <label class="control-label">নামজারী খতিয়ান নাম্বার </label>
   <div class="col-sm-3">
                   <input type="text" placeholder="নামজারী খতিয়ান নাম্বার" class="span11" value="{{$row->khotiyan_no}}" name="khotiyan_no" required="required">
                </div>
              </div>

         
                
             
          
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
               
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="span2"></div>

    <hr>
    
</div>
</div>
@endsection