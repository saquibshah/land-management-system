@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            দাগ রেজিস্টার  <button onclick="window.print()" value="Print" title="Print" class="btn btn-primary" >Print</button>
          </div>
         
          <div class="widget-content nopadding">
              @foreach($dagdetails as $dagdel)
            <table class="table table-bordered data-table">
            
                <tr style="font-size: 16px; color: black">
                 <tr>
                   <td><p>সাফ কবলা ফাইল নং : </p></td>
                   <td><p>{{ $dagdel-> saf_kobola_number}}</p></td>
                  </tr>
                  <tr>
                   <td><p>দাতা/দাত্রীর নাম : </p></td>
                   <td><p>{{ $dagdel-> data_or_datrir_name}}</p></td>
                  </tr>
                  <tr>
                     <td><p>গ্রহীতার নাম : </p></td>
                     <td><p>{{ $dagdel-> grohitar_name}}</p></td>
                  </tr>
                  <tr>
                     <td><p>মিডিয়ার নাম : </p></td>
                     <td><p>{{ $dagdel-> name_of_media}}</p></td>
                  </tr>
                   <tr>
                     <td><p>দলিল নং: </p></td>
                     <td><p>{{ $dagdel-> dolil_number}}</p></td>
                  </tr>
                <tr>
                   <td><p>তারিখ: </p></td>
                   <td><p>{{ $dagdel-> date}}</p></td>
                </tr>
                  <tr>
                   <td><p>মৌজা: </p></td>
                   <td><p>{{ $dagdel-> moujaname}}</p></td>
                </tr>
                  <tr>
                     <td><p>এস এ দাগ নং: </p></td>
                     <td><p>{{ $dagdel-> sadag_no}}</p></td>
                  </tr>
                   <tr>
                     <td><p>আর এস দাগ নং: </p></td>
                     <td><p>{{ $dagdel-> rs_dag_no}}</p></td>
                  </tr>
                 <tr>
                   <td><p>আর এস দাগে <br> মোট পরিমাণ (শতাংশ): </p></td>
                   <td><p>{{ $dagdel-> rs_dag_a_poriman}}</p></td>
                </tr>
                  <tr>
                     <td><p>খাস জমি (শতাংশ): </p></td>
                     <td><p>{{ $dagdel-> kas_jomi}}</p></td>
                  </tr>
                 <tr>
                   <td><p>অধিগ্রহণ কৃত জমি(শতাংশ): </p></td>
                   <td><p>{{ $dagdel-> odigrohan_krrito_jomi}}</p></td>
                </tr>
                <tr>
                   <td><p>R/S দাগে মোট <br> ক্রয়যোগ্য জমি  (শতাংশ): </p></td>
                   <td><p>{{ $dagdel-> rs_dagay_total_jomi}}</p></td>
                </tr>
                 <tr>
                   <td><p>R/S দাগে মোট <br> অবশিষ্ট ক্রয়যোগ্য জমি  (শতাংশ): </p></td>
                   <td><p></p></td>
                </tr>
                  <tr>
                   <td><p>ক্রয়কৃত জমির পরিমাণ (শতাংশ): </p></td>
                   <td><p>{{ $dagdel-> total_purchased_land}}</p></td>
                </tr>
                  <tr>
                   <td><p>অবশিষ্ট জমির পরিমাণ (শতাংশ): </p></td>
                   <td><p>{{ $dagdel-> remaining_land}}</p></td>
                </tr>
                  <tr>
                     <td><p>সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি ((শতাংশ): </p></td>
                     <td><p>{{ $dagdel-> smonnoy_extra_purchased_land}}</p></td>
                  </tr>
                  <tr>
                     <td><p>বায়নাকৃত জমির পরিমাণ (শতাংশ): </p></td>
                     <td><p>{{ $dagdel-> baynakrito_jomi}}</p></td>
                  </tr>
                  <tr>
                     <td><p>নামজারীকৃত জমি (শতাংশ): </p></td>
                     <td><p>{{ $dagdel-> namjari_krito_land}}</p></td>
                  </tr>
                  <tr>
                     <td><p>নামজারী বিহীন জমি (শতাংশ): </p></td>
                     <td><p>{{ $dagdel-> namjari_bihin_land}}</p></td>
                  </tr>
                  <tr>
                   <td><p>জোত নাম্বার: </p></td>
                   <td><p>{{ $dagdel-> jooth_number}}</p></td>
                </tr>
                </tr>
          </table>
                @endforeach
          </div>
        </div>       
      </div>
       <div class="span2"></div>
    </div>
  </div>


@endsection