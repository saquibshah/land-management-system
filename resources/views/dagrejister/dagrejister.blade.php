@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>দাগ রেজিস্টার</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                  <th>আর এস দাগ নং</th>      
                  <th>সাফ কবলা ফাইল নং </th>       
                  <th>দাতা/দাত্রীর নাম </th>
                  <th>গ্রহীতার নাম </th>
                  <th>মৌজা</th>
                  <th>ক্রিয়া সমূহ</th>
                </tr>
              </thead>
              <tbody>

          		@foreach($dagrejisters as $dag)
                <tr class="gradeX">
                    <td class="center">{{ $dag-> rs_dag_no}}</td>
                    <td class="center">{{ $dag-> saf_kobola_number}}</td>
                    <td class="center">{{ $dag-> data_or_datrir_name}}</td>
                    <td class="center">{{ $dag-> grohitar_name}}</td>
                    <td class="center">{{ $dag-> moujaname}}</td>
                    
                    <td>
                      <a href='dagrejister/details/{{$dag->id}}' class="btn btn-xs btn-info" title="Details"> <i class="fa fa-eye"></i> </a>
                      <a href='dagrejister/edit/{{$dag->id}}' class='btn btn-xs btn-primary'><i class='fa fa-edit'></i></a>
                      <a href='dagrejister/delete/{{$dag->id}}' class='btn btn-xs btn-danger'><i class='fa fa-trash-o' onClick='return doconfirm();'></i></a>
                    </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>

<script>
  function doconfirm()
    {
        job=confirm("আপনি কি নিশ্চিত যে আপনি এই রেকর্ড মুছে দিতে চান?");
        if(job!=true)
        {
            return false;
        }
    }
</script>
@endsection