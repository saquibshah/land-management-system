@extends('layouts.master')
@section('content')
<div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>দাগ রেজিস্টার</h5>
          </div>
          <div class="widget-content nopadding"><br>
           <form class="form-horizontal"  action="{{url('dagrejister/create')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                  <div class="form-group">
                    <label class="control-label">সাফ কবলা ফাইল নং</label>
                       <div class="col-sm-3">
                          <input type="text" placeholder="সাফ কবলা ফাইল নং" class="span11" name="saf_kobola_number" required="required">
                      </div>
                      <label class="control-label">দাতা/দাত্রীর নাম</label>
                  <div class="col-sm-3">
                  <input type="text"  placeholder="দাতা/দাত্রীর নাম" class="span11" name="data_or_datrir_name" required="required">
                </div>
                </div>
              
              
              <div class="form-group">
                <label class="control-label">গ্রহীতার নাম</label>
                  <?php
                    $all_name = DB::table('grohita')->get();
                  ?>
                  <div class="col-sm-3">
                    <select  name="grohitar_name" class="span11" required="required">
                        <option value="">গ্রহীতার নাম</option>
                        @foreach ($all_name as $v_name) 
                        <option value="{{$v_name->grohitar_name}}"><?php echo $v_name->grohitar_name; ?></option>
                         @endforeach
                    </select>
                  </div>
                  <label class="control-label">মিডিয়ার নাম</label>
              <div class="col-sm-3">
                 <input type="text" placeholder="মিডিয়ার নাম"  class="span11" id="name_of_media" name="name_of_media" required="required" >
                </div>
              </div>
              
               <div class="form-group">
                <label class="control-label">দলিল নং</label>
              <div class="col-sm-3">
                 <input type="text" placeholder="দলিল নং" class="span11"  name="dolil_number" required="required">
                </div>
               <label class="control-label">তারিখ</label>
                 <div class="col-sm-3">
                  <input type="date"  class="span11"  name="date" >
                </div>
               
              </div>
              
             
              
               <div class="form-group">
                <label class="control-label">মৌজা</label>
                  <?php
                    $all_name = DB::table('mouja')->get();
                  ?>
                  <div class="col-sm-3">
                    <select  name="mouja" class="span11" id="mouja">
                        <option value="">মৌজার নাম</option>
                        @foreach ($all_name as $v_name) 
                          <option value="{{$v_name->id}}"><?php echo $v_name->nameOfMouja; ?></option>
                        @endforeach
                    </select>
                  </div>
              <label class="control-label">আর এস দাগ নং</label>
               <div class="col-sm-3">
                 <select id="rs_dag_no" class="span11" name="rs_dag_no">
                    <option value="">আর এস দাগ নং</option>
                  </select>
                </div>
                
              </div>
             
              <div class="form-group">
              <label class="control-label">এস এ দাগ নং</label>
                  <div class="col-sm-3">
                  <input type="text" placeholder="এস এ দাগ নং" class="span11" name="sadag_no" id="sadag_no" readonly="readonly">
                </div>
                <label class="control-label">আর এস দাগে মোট পরিমাণ (শতাংশ)</label>
               <div class="col-sm-3">
                   <input type="text" placeholder="আর এস দাগে মোট পরিমাণ" class="span11" name="rs_dag_a_poriman" id="rs_dag_a_poriman" readonly="readonly">
                </div>
              </div>
               <div class="form-group">
                <label class="control-label">খাস জমি (শতাংশ)</label>
               <div class="col-sm-3">
                  <input type="text" placeholder="খাস জমি" class="span11" name="kas_jomi" id="kas_jomi" readonly="readonly">
                </div>
                <label class="control-label">অধিগ্রহণ কৃত জমি(শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="অধিগ্রহণ কৃত জমি" class="span11" name="odigrohan_krrito_jomi" id="odigrohan_krrito_jomi" readonly="readonly">
                </div>
              </div>
           
                
            
              <div class="form-group">
                <label class="control-label">R/S দাগে মোট ক্রয়যোগ্য<br> জমি (শতাংশ)</label>
                <div class="col-sm-3">
                  <input type="text" placeholder="R/S দাগে মোট ক্রয়যোগ্য জমি" class="span11" name="rs_dagay_total_jomi" id="rs_dagay_total_jomi" readonly="readonly">
                </div>
              </div>
             
              <div class="form-group">
                <label class="control-label">R/S দাগে পূর্বে মোট<br>ক্রয়কৃত জমি (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="ক্রয়কৃত জমির পরিমাণ" class="span11" id="totalRegister" readonly="readonly">
                </div>

                <label class="control-label">R/S সর্বমোট ক্রয়যোগ্য<br>জমির পরিমাণ (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="ক্রয়কৃত জমির পরিমাণ" class="span11" name="rs_remaing_land" id="rs_remaing_land" required="required" readonly="readonly">
                </div>
              </div> 
             
              <div class="form-group">
                <label class="control-label">ক্রয়কৃত জমির পরিমাণ (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="ক্রয়কৃত জমির পরিমাণ" class="span11" name="total_purchased_land" id="total_purchased_land" required="required" onchange="landValidation()">
                </div>
              </div> 
         
              <!--<div class="control-group">
                <label class="control-label">সর্বমোট ক্রয়কৃত জমির পরিমাণ (শতাংশ)</label>
                <div class="controls">
                   <input type="text" placeholder="সর্বমোট ক্রয়কৃত জমির পরিমাণ (শতাংশ)" class="span11" name="" required="required">
                </div>
              </div>-->
            <div class="form-group">
                <label class="control-label">বায়নাকৃত জমির পরিমাণ (শতাংশ)</label>
                <div class="col-sm-3">
                   <input type="text" placeholder="বায়নাকৃত জমির পরিমাণ" class="span11" name="baynakrito_jomi" required="required">
                </div>
                <label class="control-label">অবশিষ্ট জমির পরিমাণ (শতাংশ)</label>
               <div class="col-sm-3">
                   <input type="text" placeholder="অবশিষ্ট জমির পরিমাণ" class="span11" name="remaining_land" id="remaining_land" required="required" readonly="readonly">
                </div>
              </div>
         
                
            
        <div class="form-group">
                <label class="control-label">সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি ((শতাংশ)</label>
     <div class="col-sm-3">
                   <input type="text" placeholder="সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি" class="span11" name="smonnoy_extra_purchased_land" id="smonnoy_extra_purchased_land" required="required">
                </div>
                <label class="control-label">নামজারীকৃত জমি (শতাংশ)</label>
           <div class="col-sm-3">
                   <input type="text" placeholder="নামজারীকৃত জমি" class="span11" name="namjari_krito_land" id="namjari_krito_land" required="required" onchange="namjari()">
                </div>
              </div>
               
                
             
               <div class="form-group">
                <label class="control-label">নামজারী বিহীন জমি <br>(শতাংশ)</label>
            <div class="col-sm-3">
                   <input type="text" placeholder="নামজারী বিহীন জমি" class="span11" name="namjari_bihin_land" id="namjari_bihin_land" required="required" readonly="readonly">
                </div>
                <label class="control-label">জোত নাম্বার</label>
   <div class="col-sm-3">
                   <input type="text" placeholder="জোত নাম্বার" class="span11" name="jooth_number" required="required">
                </div>
              </div>

               <div class="form-group">
                <label class="control-label">নামজারী কেইস নাম্বার  </label>
            <div class="col-sm-3">
                   <input type="text" placeholder="নামজারী কেইস নাম্বার" class="span11" name="keis_no" id="keis_no" required="required">
                </div>
                <label class="control-label">নামজারী খতিয়ান নাম্বার </label>
   <div class="col-sm-3">
                   <input type="text" placeholder="নামজারী খতিয়ান নাম্বার" class="span11" name="khotiyan_no" required="required">
                </div>
              </div>

                

                
             
          
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
               
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="span2"></div>

    <hr>
    

@endsection