<!DOCTYPE html>
<html lang="en">
<head>
<title>Innovative Land Management System</title>
<meta charset="UTF-8" />
  <title></title>
</head>
<body>
 <button onclick="window.print()" value="Print" title="Print" >Print</button>
  <center>
    <table>
      <tr>
        <td style="text-align: center;">দাগ রেজিস্টার</td>
      </tr>
      <tr>
        <td style="text-align: center;">Innovative Holdings Ltd. (Purbachal East Wood City)</td>
      </tr>
      <tr>
        <td style="text-align: center;">House # 47 (7th Floor) Road # 27, Block # A, Banani, Dhaka- 1213.</td>
      </tr>
    </table> 
  </center>          
  <table border="1" cellpadding="0" cellspacing="0">
    <thead>
      <tr  style="color: black; border: 1px solid black">
      <td align="center">সাফ কবলা ফাইল নং</td>
      <td align="center"> দাতা/দাত্রীর নাম </td>

      <td align="center">মিডিয়ার নাম</td>
      <td align="center">দলিল নং</td>
      <td align="center" style="width:90px;">তারিখ</td>
      <td align="center">মৌজা</td>
      <td align="center">এস এ দাগ নং</td>
      <td align="center"> আর এস দাগ নং</td>
      <td align="center">আর এস দাগে <br> মোট পরিমাণ (শতাংশ)</td>
      <td align="center">সরজমিনে মোট জমি (শতাংশ) </td>
      <td align="center">খাস জমি (শতাংশ) </td>
      <td align="center">অধিগ্রহণ কৃত জমি(শতাংশ)</td>
      <td align="center">R/S দাগে মোট ক্রয়যোগ্য জমি  (শতাংশ) </td>
      <td align="center">ক্রয়কৃত জমির পরিমাণ (শতাংশ)</td>
      <td align="center">অবশিষ্ট জমির পরিমাণ (শতাংশ)</td>
      <td align="center">সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি (শতাংশ) </td>

      <td align="center">নামজারীকৃত জমি (শতাংশ)</td>
      <td align="center">নামজারী বিহীন জমি (শতাংশ)</td> 
      <td align="center">অতিরিক্ত নামজারী জমি (শতাংশ)</td> 
      <td align="center"> জোত নাম্বার</td>
      <td align="center"> নামজারী কেইস নাম্বার</td>
      <td align="center"> নামজারী খতিয়ান নাম্বার </td>
      </tr>
    </thead>  
		<tbody>
      <?php
      $sadag_noChk = '';
      $purchasedLand[0] = '';
      $total_rs_dag_a_poriman = 0;
      $total_sorojomine_jomi = 0;
      $total_kas_jomi = 0;
      $total_odigrohan_krrito_jomi = 0;
      $total_rs_dagay_total_jomi = 0;

      $totalPurchasedLand = 0;
      $total_rs_dag_remaing_land = 0;
      $total_remaining_land = 0;
      $total_smonnoy_extra_purchased_land = 0;
      $total_namjari_krito_land = 0;
      $total_namjariBihinPlus = 0;
      $total_namjariBihinMinus = 0;
     
      foreach($dagrejisters as $dagrpt){

        $dagNetJomi = ((float)$dagrpt->rs_dag_total_amount-(float)$dagrpt->kas_jomi-(float)$dagrpt->odigrohan_krrito_jomi);

        $total_rs_dag_a_poriman += ($sadag_noChk!=$dagrpt->rs_dag_no)?(float)$dagrpt->rs_dag_total_amount:0;
        $total_sorojomine_jomi += ($sadag_noChk!=$dagrpt->rs_dag_no)?(float)$dagrpt->sorojomine_jomi:0;
        $total_kas_jomi += ($sadag_noChk!=$dagrpt->rs_dag_no)?(float)$dagrpt->kas_jomi:0;
        $total_odigrohan_krrito_jomi += ($sadag_noChk!=$dagrpt->rs_dag_no)?(float)$dagrpt->odigrohan_krrito_jomi:0;
        $total_rs_dagay_total_jomi += ($sadag_noChk!=$dagrpt->rs_dag_no)?$dagNetJomi:0;
        $total_rs_dag_remaing_land += (float)$dagrpt->rs_dag_remaing_land;
        $totalPurchasedLand += $dagrpt->total_purchased_land;
        $total_remaining_land += (float)$dagrpt->remaining_land;
        $total_smonnoy_extra_purchased_land += $dagrpt->smonnoy_extra_purchased_land;
        $total_namjari_krito_land += $dagrpt->namjari_krito_land;

        $dagPoriman = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->rs_dag_total_amount):'';
        $dagSorojomineJomi = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->sorojomine_jomi):'';
        $dagKasJomi = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->kas_jomi):'';
        $dagOdigrohan = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->odigrohan_krrito_jomi):'';

        $namjariBihinMinus = ($dagrpt->namjari_bihin_land<0)?numberFormat($dagrpt->namjari_bihin_land):0;
        $total_namjariBihinPlus += $dagrpt->namjari_bihin_land;
        $total_namjariBihinMinus += $namjariBihinMinus;

        $purchasedLandTxt = (isset($purchasedLand[$dagrpt->rs_dag_no]))?$purchasedLand[$dagrpt->rs_dag_no]:0;
      ?>
      <tr class="gradeX">
        <td>{{ $dagrpt->saf_kobola_number}}</td>
        <td>{{ $dagrpt->data_or_datrir_name}}</td>
    
        <td align="center">{{ $dagrpt->name_of_media}}</td>
        <td align="center">{{ $dagrpt->dolil_number}}</td>
        <td align="center">{{ dateFormat($dagrpt->date)}}</td>
        <td align="center">{{ $dagrpt->nameOfMouja}}</td>
        <td align="center">{{ $dagrpt->sadag_no}}</td>
        <td align="center">{{ $dagrpt->rs_dag_no}}</td>
        <td align="right">{{ $dagPoriman }}</td>
        <td align="right">{{ $dagSorojomineJomi }}</td>
        <td align="right">{{ $dagKasJomi }}</td>
        <td align="right">{{ $dagOdigrohan }}</td>
        <td align="right">{{ numberFormat($dagNetJomi-$purchasedLandTxt)}}</td>
        <td align="right">{{ numberFormat($dagrpt->total_purchased_land)}}</td>
       
     
        <td align="right">{{ numberFormat(($dagNetJomi-$purchasedLandTxt)-$dagrpt->total_purchased_land)}}</td>
        <td align="right">{{ numberFormat($dagrpt->smonnoy_extra_purchased_land)}} </td>
   
    
        <td align="right">{{ numberFormat($dagrpt->namjari_krito_land)}}</td>
        <td align="right">{{ numberFormat($dagrpt->namjari_bihin_land)}}</td>
        <td align="right">{{ numberFormat($namjariBihinMinus) }}</td>
        <td align="center">{{ $dagrpt->jooth_number}}</td>
        <td align="center">{{ $dagrpt->keis_no}}</td>
        <td align="center">{{ $dagrpt->khotiyan_no}}</td>
      </tr>
      <?php
        $sadag_noChk = $dagrpt->rs_dag_no;
        @$purchasedLand[$dagrpt->rs_dag_no] += $dagrpt->total_purchased_land;
      }
      ?>
    </tbody>
    <tfoot>
      <tr style="color: black; font-weight: bold;">
        <td colspan="8">Total</td>
        <td align="right">{{ numberFormat($total_rs_dag_a_poriman) }}</td>
        <td align="right">{{ numberFormat($total_sorojomine_jomi) }}</td>
        <td align="right">{{ numberFormat($total_kas_jomi) }}</td>
        <td align="right">{{ numberFormat($total_odigrohan_krrito_jomi) }}</td>
        <td align="right">{{ numberFormat($total_rs_dagay_total_jomi) }}</td>
        <td align="right">{{ numberFormat($totalPurchasedLand) }}</td>
        <td align="right">{{ numberFormat($total_rs_dagay_total_jomi-$totalPurchasedLand) }}</td>
        <td align="right">{{ numberFormat($total_smonnoy_extra_purchased_land) }}</td>
        <td align="right">{{ numberFormat($total_namjari_krito_land) }}</td>
        <td align="right">{{ numberFormat($total_namjariBihinPlus) }}</td>
        <td align="right">{{ numberFormat($total_namjariBihinMinus) }}</td>
        <td colspan="3"></td>
      </tr>
    </tfoot>
  </table>
</body>
</html>