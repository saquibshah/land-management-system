<!DOCTYPE html>
<html lang="en">
<head>
<title>Innovative Land Management System</title>
<meta charset="UTF-8" />
<meta name="viewport"  content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="icon"       href="{{asset('admin/img/logonw.png')}}" type="image/x-icon">
<link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/bootstrap-responsive.min.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/colorpicker.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/datepicker.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/uniform.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/select2.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/maruti-style.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/print.css')}}" />
<link rel="stylesheet" href="{{asset('admin/css/maruti-media.css')}}" class="skin-color" />
<style>
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<!--Header-part-->
<div id="header">
 
</div>

<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li class=""><a title="" href="{{ url('/logout')}}"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>

<!--close-top-Header-menu-->

<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> ড্যাশবোর্ড</a><ul>
  <li><a href="{{asset('/')}}"><i class="icon icon-home"></i> <span>ড্যাশবোর্ড</span></a> </li>
    
    
   
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>দাগ রেজিস্টার</span> <span class="label label-important"></span></a>
      <ul>
        <li><a href="{{asset('/dagrejister/create')}}">দাগ রেজিস্টার যুক্ত করুন</a></li>
        <li><a href="{{asset('/dagrejister')}}">দাগ রেজিস্টার দেখুন</a></li>
        <!--<li><a href="{{asset('/purtoexcel')}}">দাগ রেজিষ্টার এক্সেল এক্সপোর্ট</a></li>-->
        <li><a href="{{asset('/dagreport')}}">দাগ রেজিষ্টার এক্সেল এক্সপোর্ট</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>ক্রয় রেজিস্টার</span></a>
      <ul>
        <li><a href="{{asset('/purchaseregister/create')}}">ক্রয় রেজিস্টার যুক্ত করুন</a></li>
        <li><a href="{{asset('/purchaseregister')}}">ক্রয় রেজিস্টার দেখুন</a></li>
        <li><a href="{{asset('/purchase')}}">ক্রয় রেজিস্টার এক্সেল এক্সপোর্ট</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>বিক্রয় রেজিস্টার</span></a>
      <ul>
        <li><a href="{{asset('/sellregister/create')}}">বিক্রয় রেজিস্টার যুক্ত করুন</a></li>
        <li><a href="{{asset('/sellregister')}}">বিক্রয় রেজিস্টার দেখুন</a></li>
        <li><a href="{{asset('/sellreport')}}">বিক্রয় রেজিস্টার  এক্সেল এক্সপোর্ট</a></li>
      </ul>
    </li>
    <!--<li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>বিস্তারিত</span></a>
      <ul>
        <li><a href="{{asset('/details')}}">বিস্তারিত দেখুন</a></li>
        <li><a href="{{asset('/search')}}">মৌজা/ আর এস দাগ/ সাফ কাবলা ফাইল আনুসারে খুজুন</a></li>
      </ul>
    </li>-->
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Export রিপোর্ট</span></a>
      <ul>
        <li><a href="{{asset('/dagexcel')}}" target="_blank">দাগ রেজিষ্টার এক্সেল এক্সপোর্ট</a></li>
        <li><a href="{{asset('/exp_excel')}}">ক্রয় রেজিস্টার এক্সেল এক্সপোর্ট</a></li>
        <li><a href="">বিক্রয় রেজিস্টার  এক্সেল এক্সপোর্ট</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>রিপোর্ট</span></a>
      <ul>
        <li><a href="{{asset('/purchase_report')}}">সর্বমোট কোম্পানির খরিদা ভূমি</a></li>
        <li><a href="{{asset('/total')}}">সর্বমোট প্রকল্প ব্যায়</a></li>
        <li><a href="{{asset('/allreport')}}">সম্পূর্ন রিপোর্ট প্রিন্ট করুন</a></li>
        <!-- <li><a href="{{asset('/details')}}">বিস্তারিত দেখুন</a></li> -->
        <li><a href="{{asset('/search')}}">মৌজা/ আর এস দাগ/ সাফ কাবলা ফাইল আনুসারে খুজুন</a></li>
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>মৌলিক সংযোজন</span></a>
      <ul>
        <li><a href="{{asset('/moujaadd')}}">মৌজা সংযোজন</a></li>
        <!--<li><a href="{{asset('/moujatoexcel')}}">মৌজা Export to Excel</a></li>-->
        <li><a href="{{asset('/moujainfo')}}">মৌজার বিভিন্ন তথ্য</a></li>
        <li><a href="{{asset('/grohita')}}">গ্রহীতা সংযোজন</a></li>
        <li><a href="{{asset('/land')}}">জমি  সংযোজন</a></li>
      </ul>
    </li>
   
    
  </ul>
</div>
<div id="content">
  <div id="content-header"></div>
  <div class="container-fluid">
  </div>
<div id="page-wrapper" >
    <section class="content">
               @yield('content')
    </section>
</div>


</div>


<!--<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy; Codex Ltd. </div>
</div>-->

<script src="{{asset('admin/js/jquery.min.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<script src="{{asset('admin/js/jquery.ui.custom.js')}}"></script> 
<script src="{{asset('admin/js/maruti.tables.js')}}"></script>
<script src="{{asset('admin/js/bootstrap-colorpicker.js')}}"></script> 
<script src="{{asset('admin/js/bootstrap-datepicker.js')}}"></script> 
<script src="{{asset('admin/js/jquery.uniform.js')}}"></script> 
<script src="{{asset('admin/js/select2.min.js')}}"></script>
<script src="{{asset('admin/js/maruti.js')}}"></script> 
<script src="{{asset('admin/js/maruti.form_common.js')}}"></script>
<script src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script> 
<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>

<script>
    $('#mouja').on('change', function(e){
        console.log(e);
        var state_id = e.target.value;
        $.get('{{ url('information') }}/create/ajax-state?state_id=' + state_id, function(data) {
            console.log(data);
            $('#rs_dag_no').empty();
            var html = '<option value="">আর এস দাগ নং</option>';
            $.each(data, function(index,subCatObj){
            html +="<option value='"+ subCatObj.rs_dag_no +"'>" + subCatObj.rs_dag_no + "</option>";
            //$('#rs_dag_id').val(subCatObj.id);
            });
            $('#rs_dag_no').append(html);
        });
    });
</script>

<script>
    $('#rs_dag_no').on('change', function(e){
        console.log(e);
        var rs_dag_no = e.target.value;
        var mouja = document.getElementById('mouja').value;
        $.get('{{ url('information') }}/create/ajax-rs-dag?rs_dag_no=' + rs_dag_no+'&mouja=' + mouja, function(data) {
            console.log(data);
            $.each(data, function(index,subCatObj){
            $('#sadag_no').val(subCatObj.sa_dag_no);
            $('#rs_dag_a_poriman').val(subCatObj.rs_dag_total_amount);
            $('#kas_jomi').val(subCatObj.kas_jomi);
            $('#odigrohan_krrito_jomi').val(subCatObj.odigrohon_jomi);
            var tot = parseInt(subCatObj.kas_jomi) + parseInt(subCatObj.odigrohon_jomi);
            var tot_val = parseInt(subCatObj.rs_dag_total_amount) - parseInt(tot);
            $('#totalRegister').val(subCatObj.totalRegister);
            $('#rs_remaing_land').val((tot_val-subCatObj.totalRegister));
            $('#rs_dagay_total_jomi').val(tot_val);
            });
        });
    });
</script>

<script>
    $('#saf_kobola_number').on('change', function(e){
        console.log(e);
        var saf_kobola_number = e.target.value;
        $.get('{{ url('information') }}/create/ajax-saf-kobola?saf_kobola_number=' + saf_kobola_number, function(data) {
            console.log(data);
            $('#korida_onso').empty();
            $.each(data, function(index,subCatObj){
            $('#korida_onso').append('<tr><td> আর এস দাগ নং : '+subCatObj.rs_dag_no+'</td><td> খরিদা অংশ : '+subCatObj.total_purchased_land+'</td></tr>');
            });
        });
    });
</script>

<script type="text/javascript">
  function landValidation()
  {
    var first = document.getElementById('rs_remaing_land').value;
    var second = document.getElementById('total_purchased_land').value;
    var rem_land = first - second;
    //alert(first);
    //alert(second);
    if (parseInt(first) < parseInt(second)) {
      document.getElementById('total_purchased_land').value='';
      alert("You have no available land");
      //document.getElementById('total_purchased_land').empty();
    }else{
      document.getElementById("remaining_land").value = rem_land;
      //alert(rem_land);
    }
  }
</script>

<script type="text/javascript">
  function namjari()
  {
    var first = document.getElementById('namjari_krito_land').value;
    var second = document.getElementById('total_purchased_land').value;
    var rem_land = second - first;
    document.getElementById("namjari_bihin_land").value = rem_land;
  }
</script>

<script type="text/javascript">
  function addInput(divName){
    //alert("test");
    var newdiv = document.createElement('div');
    newdiv.innerHTML = "<div class='form-group' id='dynamicInput'><label class='control-label'>আর এস দাগ নং</label><div class='col-sm-3'><input type='text' name='rs_dag_no[]' id='rs_dag_no[]' class='form-control'></div><label class='control-label'>খরিদা অংশ</label><div class='col-sm-3'><input type='text' name='korida_onso[]' id='korida_onso[]' class='form-control'></div></div>";
    document.getElementById(divName).appendChild(newdiv);
  }
</script>
<script type="text/javascript">
  function linebazet() {
    var a = document.getElementById("toatal_price_of_purchased_land").value;
    var g= document.getElementById("total_budget_money").value;
    var c = document.getElementById("registration_cost").value;
    var d= document.getElementById("meadia_commision").value;
    var e= document.getElementById("anosongik_cost").value;
    //alert ("test");
    var f = (parseInt(a)+parseInt(c)+parseInt(d)+parseInt(e));
    //var f = (a+b+c+d+e);
    //alert (f);
    document.getElementById("total_cost").value = f;
    if (parseInt(f) < parseInt(g)) {
      alert("Total cost can not be greater than budget money");
     
    }
    finalbazet();
  }
  function finalbazet() {
   
    var g= document.getElementById("total_budget_money").value;
    var f= document.getElementById("total_cost").value 

    var h = parseInt(g)-parseInt(f);
    //alert (h);
    document.getElementById("returned_money").value = h;


  }

</script> 

<script type="text/javascript">
function calculate(id)
{
  var bina_total_budget_money = Number($('#bina_total_budget_money').val());
  if(isNaN(bina_total_budget_money)){
    $('#bina_total_budget_money').val('');
    $('#bina_total_budget_money').focus();
    alert('Please Provide Valid Number.');
    bina_total_budget_money = 0;
  }

  var bina_datak_prodan = Number($('#bina_datak_prodan').val());
  if(isNaN(bina_datak_prodan)){
    $('#bina_datak_prodan').val('');
    $('#bina_datak_prodan').focus();
    alert('Please Provide Valid Number.');
    bina_datak_prodan = 0;
  }
  var bina_registration_cost = Number($('#bina_registration_cost').val());
  if(isNaN(bina_registration_cost)){
    $('#bina_registration_cost').val('');
    $('#bina_registration_cost').focus();
    alert('Please Provide Valid Number.');
    bina_registration_cost = 0;
  }
  var bina_meadia_commision = Number($('#bina_meadia_commision').val());
  if(isNaN(bina_meadia_commision)){
    $('#bina_meadia_commision').val('');
    $('#bina_meadia_commision').focus();
    alert('Please Provide Valid Number.');
    bina_meadia_commision = 0;
  }
  var bina_anosongik_cost = Number($('#bina_anosongik_cost').val());
  if(isNaN(bina_anosongik_cost)){
    $('#bina_anosongik_cost').val('');
    $('#bina_anosongik_cost').focus();
    alert('Please Provide Valid Number.');
    bina_anosongik_cost = 0;
  }
  var bina_total_cost = (bina_datak_prodan+bina_registration_cost+bina_meadia_commision+bina_anosongik_cost);
  var bina_returned_money = (bina_total_budget_money-bina_total_cost);
  $('#bina_total_cost').val(bina_total_cost);
  $('#bina_returned_money').val(bina_returned_money);

  if(bina_returned_money<0){
    $('#bina_total_cost').val('');
    $('#bina_returned_money').val('');

    if(id=='bina_total_budget_money'){
      $('#bina_datak_prodan').val('');
      $('#bina_registration_cost').val('');
      $('#bina_meadia_commision').val('');
      $('#bina_anosongik_cost').val('');
    }
    else{
      $('#'+id).val('');
    }
    alert("Total cost can not be greater than budget money");
  }
  final_cal();
}
</script>

<script type="text/javascript">
function sab_calculate(id)
{
   var total_budget_money = Number($('#total_budget_money').val());
  if(isNaN(total_budget_money)){
    $('#total_budget_money').val('');
    $('#total_budget_money').focus();
    alert('Please Provide Valid Number.');
    total_budget_money = 0;
  }

   var toatal_price_of_purchased_land = Number($('#toatal_price_of_purchased_land').val());
  if(isNaN(toatal_price_of_purchased_land)){
    $('#toatal_price_of_purchased_land').val('');
    $('#toatal_price_of_purchased_land').focus();
    alert('Please Provide Valid Number.');
    toatal_price_of_purchased_land = 0;
  }

  var registration_cost = Number($('#registration_cost').val());
  if(isNaN(registration_cost)){
    $('#registration_cost').val('');
    $('#registration_cost').focus();
    alert('Please Provide Valid Number.');
    registration_cost = 0;
  }

   var meadia_commision = Number($('#meadia_commision').val());
  if(isNaN(meadia_commision)){
    $('#meadia_commision').val('');
    $('#meadia_commision').focus();
    alert('Please Provide Valid Number.');
    meadia_commision = 0;
  }

  var anosongik_cost = Number($('#anosongik_cost').val());
  if(isNaN(anosongik_cost)){
    $('#anosongik_cost').val('');
    $('#anosongik_cost').focus();
    alert('Please Provide Valid Number.');
    anosongik_cost = 0;
  }

   var total_cost = (toatal_price_of_purchased_land+registration_cost+meadia_commision+anosongik_cost);
  var returned_money = (total_budget_money-total_cost);
  $('#total_cost').val(total_cost);
  $('#returned_money').val(returned_money);

  if(returned_money<0){
    $('#total_cost').val('');
    $('#returned_money').val('');

    if(id=='total_budget_money'){
      $('#toatal_price_of_purchased_land').val('');
      $('#registration_cost').val('');
      $('#meadia_commision').val('');
      $('#anosongik_cost').val('');
    }
    else{
      $('#'+id).val('');
    }
    alert("Total cost can not be greater than budget money");
  }

 
  final_cal();
}

function final_cal()
{
  var final_total_result = document.getElementById('sorbomot');
  var bina_total_money = document.getElementById('bina_total_cost').value;
  var total_cost = document.getElementById('total_cost').value;
  final_total_result.value = Number(bina_total_money) + Number(total_cost);
  if(document.getElementById('bina_total_cost').value =="" && document.getElementById('total_cost').value ==""){
   final_total_result.value ="";
  }
}
</script>

<script type="text/javascript">
 function lineBaina() {
    var i = document.getElementById("bina_datak_prodan").value;
    var j = document.getElementById("bina_registration_cost").value;
    var k = document.getElementById("bina_meadia_commision").value;
    var l= document.getElementById("bina_anosongik_cost").value;
    var n= document.getElementById("bina_total_budget_money").value;

    if (i = '') {}
    var m = (parseInt(i)+parseInt(j)+parseInt(k)+parseInt(l));
    
    //alert (f);
    document.getElementById("bina_total_cost").value = m;

     if (parseInt(m) < parseInt(n)) {
      alert("Total cost can not be greater than budget money");
     
    }
    finalBaina();
  }
  function finalBaina() {
   
    var n= document.getElementById("bina_total_budget_money").value;
    var m= document.getElementById("bina_total_cost").value 


    var o = parseInt(n)-parseInt(m);
    //alert (f);
    document.getElementById("bina_returned_money").value = o;
  }
</script>
<script type="text/javascript">
  function  finalkhoros(){
  var m= document.getElementById("bina_total_cost").value 
  var f= document.getElementById("total_cost").value 
  var r = parseInt(m)+parseInt(f);
  //alert (r);
  document.getElementById("sorbomot").value = r;
      
  }
</script>


@yield('script')
</body>
</html>
