@extends('layouts.master')
@section('content')
<div id="content">
  <div id="content-header"></div>
  <div class="container-fluid">
  
    <div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>মৌজা সংযোজন</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal"  action="{{url('moujaadd/update')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                 <input type="hidden" name="txtId" value="{{$row->id}}" class="form-control" readonly /> 
              <div class="control-group">

                <label class="control-label">মৌজার নাম</label>
                <div class="controls">
                  <input type="text"  placeholder="মৌজার নাম" value="{{$row->nameOfMouja}}" class="colorpicker input-big span11" name="nameOfMouja" required="required">
                 </div>
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
                
              </div>
              </form>
          </div>
        </div>
      </div>
    </div>
    <div class="span2"></div>

    <hr>
    
</div>
</div>
</div>

@endsection