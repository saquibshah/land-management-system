@extends('layouts.master')
@section('content')


  <div class="container">
    <div class="row">
     <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <a href="{{url('moujaadd/create')}}" class="btn btn-primary">+মৌজা সংযোজন</a>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                <th>ক্রম নং</th>
                   <th>মৌজার নাম</th>      
                  
                  <th>ক্রিয়া সমূহ</th>
                </tr>
              </thead>
              <tbody>
              <?php $i=1; ?>
          		@foreach($moujas as $moja)
                <tr class="gradeX">
                  <td>{{ $i++}}</td>
                    <td class="center">{{ $moja-> nameOfMouja}}</td>
                    
                    <td>
              <a href='moujaadd/edit/{{$moja->id}}' class='btn btn-xs btn-success'><i class='fa fa-edit'>পুনরায় ঠিক করুন</i></a>
              <a href='moujaadd/delete/{{$moja->id}}' class='btn btn-xs btn-danger'><i class='fa fa-trash-o' onClick='return doconfirm();'>মুছে ফেলুন</i></a>
            </td>
                </tr>
                 @endforeach
              </tbody>
            </table>
          </div>
        </div>       
      </div>
           <div class="span2"></div>
    </div>
  </div>

<script>
  function doconfirm()
    {
        job=confirm("আপনি কি নিশ্চিত যে আপনি এই রেকর্ড মুছে দিতে চান?");
        if(job!=true)
        {
            return false;
        }
    }
</script>
@endsection