@extends('layouts.master')
@section('content')
<div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>বিক্রয় রেজিস্টার যুক্ত করুন</h5>
          </div>
          <div class="widget-content nopadding"><br>
            <form class="form-horizontal"  action="{{url('sellregister/update')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                  <input type="hidden" name="txtId" value="{{$row->id}}" class="form-control" readonly /> 
                <div class="form-group">
                  <label class="control-label">প্লট বিক্রয় ফাইল নং</label>
                    <div class="col-sm-3">
                      <input type="text" placeholder="প্লট বিক্রয় ফাইল নং" class="span11" name="plot_sell_file_number" required="required" value="{{$row->plot_sell_file_number}}">
                    </div>
                    <label class="control-label">প্লট আইডি নং</label>
                    <div class="col-sm-3">
                      <input type="text" placeholder="প্লট আইডি নং" class="span11"  name="plot_id_number" required="required" value="{{$row->plot_id_number}}">
                      </div>
                </div>
                <div class="form-group">
                   <label class="control-label">ব্লক নাম্বার</label>
                    <?php
                      $all_name = DB::table('land_details')
                                ->select('block_name')
                                ->groupBy('block_name')
                                ->get();
                      ?>
                      <div class="col-sm-3">
                        <select  name="block_name" class="span11" id="block_name" onchange="getRoadNo()">
                          <option value="">ব্লক নাম্বার</option>
                            @foreach ($all_name as $land_no)
                            <?php $sel = ($row->block_name==$land_no->block_name)?'selected':''?> 
                            <option value="{{$land_no->block_name}}" {{$sel}}>{{$land_no->block_name}}</option>
                            @endforeach
                        </select>
                      </div>
                  <label class="control-label">রোড নাম্বার</label>
                  <?php
                
                  $roadSql = DB::table('land_details')
                                ->select('road_number')
                                ->where('block_name','=',$row->block_name)
                                ->groupBy('road_number')
                                ->get();
                  ?>
                    <div class="col-sm-3">
                      <select class="span11" name="road_number" id="road_number" onchange="getPlot()">
                          <option value="">রোড নাম্বার</option>
                            @foreach ($roadSql as $land_no)
                            <?php $sel = ($row->road_number==$land_no->road_number)?'selected':''?> 
                            <option value="{{urldecode($land_no->road_number)}}" {{$sel}}>{{$land_no->road_number}}</option>
                            @endforeach
                       </select>
                    </div>
                </div>
                <div class="form-group">
                   <label class="control-label">কোন মুখী</label>
                    <div class="col-sm-3">
                      <input type="text" placeholder="কোন মুখী" class="span11" name="face" id="face" readonly value="{{$row->face}}">
                    </div>

                  <label class="control-label">প্লট নাম্বার</label>
                  <?php 
                     $plotNo = DB::table('land_details')
                                    ->select('face','plot_number')
                                    ->where('block_name','=',$row->block_name)
                                    ->where('road_number','=',$row->road_number)
                                    ->groupBy('plot_number')
                                    ->get();

                  ?>
                  <div class="col-sm-3">
                      <select class="span11" name="plot_number" id="plot_number" onchange="getSize()">
                          <option value="">প্লট নাম্বার</option>
                          @foreach ($plotNo as $land_no)
                            <?php $sel = ($row->plot_number==$land_no->plot_number)?'selected':''?> 
                            <option value="{{$land_no->plot_number}}" {{$sel}}>{{$land_no->plot_number}}</option>
                            @endforeach
                       </select>
                    </div>

                </div> 
                <div class="form-group">
                  <label class="control-label">প্লট সাইজ</label>
                  <div class="col-sm-3">
                      <input type="text" placeholder="প্লট সাইজ" class="span11"  name="plot_size" id="plot_size" readonly value="{{$row->plot_size}}">
                      <input type="hidden" name="land_id" id="land_id" required value="{{$row->land_id}}">
                  </div>
                  <label class="control-label">ক্রেতার নাম</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="ক্রেতার নাম" class="span11" name="buyer_name" required="required" value="{{$row->buyer_name}}">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">মধ্যস্থতাকারীর নাম</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="মধ্যস্থতাকারীর নাম" class="span11" name="media_name" required="required" value="{{$row->media_name}}">
                  </div>
                  <label class="control-label">মৌজা</label>
                  <?php
                    $all_name = DB::table('mouja')->get();
                  ?>
                  <div class="col-sm-3">
                    <select  name="mouja_id" class="span11" id="mouja_id" onchange="getRS()">
                        <option value="">মৌজার নাম</option>
                        @foreach ($all_name as $v_name) 
                          <option value="{{$v_name->id}}" <?php if ($row->mouja_id == $v_name->id) echo ' selected="selected"';?>><?php echo $v_name->nameOfMouja; ?></option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                 <label class="control-label">আর এস দাগ নং</label>
               <div class="col-sm-3">
                 <select class="span11" name="rs_dag_no" id="rsdagno" onchange="getSafKobola()">
                   <option value="">আর এস দাগ নং</option>
                    <?php
                    $all_dag = DB::table('mouja_dag')->where('mouja_id',$row->mouja_id)->get();
                    ?>
                    @foreach ($all_dag as $dag) 
                      <option value="{{$dag->rs_dag_no}}"  <?php if ($row->rs_dag_no == $dag->rs_dag_no) echo ' selected="selected"';?>><?php echo $dag->rs_dag_no ?></option>
                    @endforeach
                  </select>
                </div>
                  <label class="control-label">R/S  দাগে বিক্রয়যোগ্য জমি</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="R/S  দাগে বিক্রয়যোগ্য জমি" class="span11" id="sellable_land" required="required" readonly value="{{$row->totalRegister}}">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label">পূর্বে বিক্রিত ভূমি </label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="পূর্বে বিক্রিত ভূমি" class="span11"  required="required" readonly id="prev_sell_land" value="{{$row->totalSell}}">
                  </div>
                 <label class="control-label">বিক্রিত ভূমি </label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="বিক্রিত ভূমি" class="span11" name="sell_land" id="sell_land" required="required" onkeyup="sellLand()" value="{{$row->sell_land}}">
                  </div>
                </div>

                <div class="form-group">
                  
                  <label class="control-label">সাফ কবলা ফাইল নং</label>
                <div class="col-sm-3">
                 <?php
                  $all_name = DB::table('dag_register')
                          ->select('saf_kobola_number')
                          ->where('mouja_id',$row->mouja_id)
                          ->where('rs_dag_no',$row->rs_dag_no)
                          ->groupBy('saf_kobola_number')
                          ->get();

                $subFileNumber = explode(', ', $row->saf_kobola_number);
                ?>
                  <select name="saf_kobola_number[]"  required="required" id="saf_kobola" class="span11" multiple>
                  <option value="">সাফ কবলা ফাইল নং</option>
                      @foreach ($all_name as $v_name) 
                        <option value="{{$v_name->saf_kobola_number}}" <?php if (in_array($v_name->saf_kobola_number, $subFileNumber)) echo ' selected="selected"';?>><?php echo $v_name->saf_kobola_number; ?></option>
                      @endforeach
                  </select>
                 </div>
                 <label class="control-label">অবিক্রিত ভূমি </label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="অবিক্রিত ভূমি" class="span11" id="remaining_land" value="{{$row->totalRegister-$row->totalSell}}" required="required" readonly  onkeyup="sell_land(this.id)">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">বিক্রিত ভূমির মূল্য </label>
                  <div class="col-sm-3">
                    <input type="text" value="{{$row->sell_land_price}}" placeholder="বিক্রিত ভূমির মূল্য" class="span11" name="sell_land_price" required="required" id="sell_land_price" onkeyup="sell_calculate(this.id)">
                  </div>

                  <label class="control-label">বিক্রিত ভূমির <br>ডেভেলপমেন্ট খরচ </label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="বিক্রিত ভূমির ডেভেলপমেন্ট খরচ" class="span11" name="sell_land_dev_cost" id="sell_land_dev_cost" required="required" onkeyup="sell_calculate(this.id)" value="{{$row->sell_land_dev_cost}}">
                  </div>
                </div>
                <div class="form-group">
                  
                  <label class="control-label">রেজিস্ট্রেশন খরচ </label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="রেজিস্ট্রেশন খরচ" class="span11" name="registry_cost" id="registry_cost" required="required" onkeyup="sell_calculate(this.id)" value="{{$row->registry_cost}}">
                  </div>
                  <label class="control-label">আনুষঙ্গিক খরচ </label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="আনুষঙ্গিক খরচ" value="{{$row->others_cost}}" class="span11" name="others_cost" id="others_cost" required="required" onkeyup="sell_calculate(this.id)">
                  </div>
                </div>

                <div class="form-group">
                  
                  <label class="control-label">মোট  খরচ </label>
                  <div class="col-sm-3">
                   <input type="text" placeholder="মোট খরচ" class="span11" value="{{$row->total_cost}}"  name="total_cost" required="required" id="total_cost" readonly>
                  </div>
                  <label class="control-label">দলিল নং </label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="দলিল নং" class="span11" name="dolil_no" required="required" value="{{$row->dolil_no}}" >
                  </div>
                </div>
                <div class="form-group">
                  
                  <label class="control-label">তারিখ </label>
                  <div class="col-sm-3">
                    <input type="date" placeholder="তারিখ" class="span11" name="sell_date" required="required" value="{{$row->sell_date}}" >
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label">মন্তব্য</label>
                  <div class="col-sm-9">
                    <textarea class="span11" required="required"  name="comment" class="form-control" size="130">{{$row->comment}}</textarea>
                  </div>
                </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
                </div>
            </form>
          </div>
        </div>
      </div>
</div>
@endsection

@section('script')
<script type="text/javascript">

  function getRoadNo()
{
  var block_name = $('#block_name').val();
  $.get('{{ url('information') }}/create/get-roadno?block_name=' + block_name, function(data) {
    console.log(data);
    $('#road_number').empty();
    var html = '<option value="">রোড নাম্বার</option>';
    $.each(data, function(index,subCatObj){
      html +="<option value='"+ subCatObj.road_number +"'>" + subCatObj.road_number + "</option>";
    });
    $('#road_number').html(html);
  });
}

function getPlot()
{
  var block_name = $('#block_name').val();
  var road_number = $('#road_number').val();
  $.get('{{ url('information') }}/create/get-plot?block_name=' + block_name+'&road_number=' + road_number, function(data) {
    console.log(data);

    $('#face').val(data[0].face);

    $('#plot_number').empty();
    var html = '<option value="">প্লট নাম্বার</option>';
    $.each(data, function(index,subCatObj){
      html +="<option value='"+ subCatObj.plot_number +"'>" + subCatObj.plot_number + "</option>";
    });
    $('#plot_number').html(html);
  });
}

function getSize()
{
  var block_name = $('#block_name').val();
  var road_number = $('#road_number').val();
  var plot_number = $('#plot_number').val();
  $.get('{{ url('information') }}/create/get-size?block_name=' + block_name+'&road_number=' + road_number+'&plot_number=' + plot_number, function(data) {
    console.log(data);

    $('#plot_size').val(data[0].plot_size);
    $('#land_id').val(data[0].id);
  });
}

function getRS()
{
  var mouja_id = $('#mouja_id').val();
  $.get('{{ url('information') }}/create/ajax-state?state_id=' + mouja_id, function(data) {
    console.log(data);
    $('#rsdagno').empty();
    var html = '<option value="">আর এস দাগ নং</option>';
    $.each(data, function(index,subCatObj){
      html +="<option value='"+ subCatObj.rs_dag_no +"'>" + subCatObj.rs_dag_no + "</option>";
    });
    $('#rsdagno').append(html);
  });
}

function getSafKobola()
{
  var mouja_id = $('#mouja_id').val();
  var rs_dag_no = $('#rsdagno').val();

  $.ajax({
     url: '{{ url('sellregister-getrs-details') }}',
     dataType: 'json',
     type: 'GET',
     data: { 'mouja_id': mouja_id, 'rs_dag_no': rs_dag_no },
     success: function(data) {
        console.log(data);
        $('#sellable_land').val(data.totalRegister);
        $('#prev_sell_land').val(data.totalSell);
        $('#saf_kobola').empty();
        var html = '<option value="">সাফ কবলা ফাইল নং</option>';
        $.each(data.allSafkobola, function(index,subCatObj){
          html +="<option value='"+ subCatObj.saf_kobola_number +"'>" + subCatObj.saf_kobola_number + "</option>";
        });
        $('#saf_kobola').append(html);
     },
     error: function() {
        //$('#info').html('<p>An error has occurred</p>');
     }
  });
}

function sell_calculate(id)
{
  var sell_land_price = Number($('#sell_land_price').val());
  if(isNaN(sell_land_price)){
    $('#sell_land_price').val('');
    $('#sell_land_price').focus();
    alert('Please Provide Valid Number.');
    sell_land_price = 0;
  }
  var sell_land_dev_cost = Number($('#sell_land_dev_cost').val());
  if(isNaN(sell_land_dev_cost)){
    $('#sell_land_dev_cost').val('');
    $('#sell_land_dev_cost').focus();
    alert('Please Provide Valid Number.');
    sell_land_dev_cost = 0;
  }

  var registry_cost = Number($('#registry_cost').val());
  if(isNaN(registry_cost)){
    $('#registry_cost').val('');
    $('#registry_cost').focus();
    alert('Please Provide Valid Number.');
    registry_cost = 0;
  }

  var others_cost = Number($('#others_cost').val());
  if(isNaN(others_cost)){
    $('#others_cost').val('');
    $('#others_cost').focus();
    alert('Please Provide Valid Number.');
    others_cost = 0;
  }

  var total_cost = (sell_land_price+sell_land_dev_cost+registry_cost+others_cost);
  $('#total_cost').val(total_cost);
}

function sellLand()
{
  var sellable_land = Number($('#sellable_land').val());
  var prev_sell_land = Number($('#prev_sell_land').val());
  var sell_land = Number($('#sell_land').val());
  if(isNaN(sell_land)){
    $('#sell_land').val('');
    $('#sell_land').focus();
    alert('Please Provide Valid Number.');
    sell_land = 0;
  }
  var remaining_land = (sellable_land-(prev_sell_land+sell_land));
  if(remaining_land<0){
    $('#sell_land').val('');
    $('#sell_land').focus();
    alert('can not greter than remaining land.');
    remaining_land=0;
  }

  $('#remaining_land').val(remaining_land);

}
</script>
@endsection