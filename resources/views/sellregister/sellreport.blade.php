@extends('layouts.master')
@section('content')

<div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
         <button onclick="window.print()" value="Print" title="Print" class="no-print" >Print</button>
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>বিক্রয় রেজিস্টার</h5>
          </div>

          <div class="widget-content nopadding">
            <center>
              <table>
                <tr>
                  <td style="text-align: center;">বিক্রয় রেজিস্টার</td>
                </tr>
                <tr>
                  <td style="text-align: center;">Innovative Holdings Ltd. (Purbachal East Wood City)</td>
                </tr>
                <tr>
                  <td style="text-align: center;">House # 47 (7th Floor) Road # 27, Block # A, Banani, Dhaka- 1213.</td>
                </tr>
              </table> 
            </center>
             <table border="1" cellpadding="0" cellspacing="0">
              <thead>
                <tr class="gradeX" style="color: black; font-weight: bold;">
                  <td align="center" >প্লট বিক্রয় ফাইল নং</td>
                  <td align="center" >প্লট আইডি নং</td>
                  <td align="center" >ব্লক নাম্বার</td>
                  <td align="center" >রোড নাম্বার</td>
                  <td align="center" >প্লট নাম্বার</td>
                  <td align="center" >কোন মুখী</td>
                  <td align="center" >ক্রেতার নাম</td>
                  <td align="center" >মধ্যস্থতাকারীর নাম</td>
                  <td align="center" >মৌজা</td>
                  <td align="center" >আর এস দাগ নং </td>
                  <td align="center" >সাফ কবলা ফাইল নং </td>
                  <td align="center" >R/S  দাগে বিক্রয়যোগ্য জমি</td>
                  <td align="center" >পূর্বে বিক্রিত ভূমি  </td>
                  <td align="center" >বিক্রিত ভূমি   </td>
                  <td align="center" >অবিক্রিত ভূমি </td>
                  <td align="center" >বিক্রিত ভূমির মূল্য </td>
                  <td align="center" >বিক্রিত ভূমির ডেভেলপমেন্ট খরচ</td>
                  <td align="center" >রেজিস্ট্রেশন খরচ </td>
                  <td align="center" >আনুষঙ্গিক খরচ </td>
                  <td align="center" >মোট  খরচ </td>
                  <td align="center" >দলিল নং </td>
                  <td align="center" >তারিখ </td>
                  <td align="center" >মন্তব্য </td>
                </tr>
              </thead>
              <tbody>
                  <?php 
                   $total_sell_land = 0;
                   $total_sell_land_price = 0;
                   $total_sell_land_dev_cost = 0;
                   $total_registry_cost = 0;
                   $total_others_cost = 0;
                   $total_total_cost = 0;
                   $total_totalRegister = 0;
                   $total_totalSell = 0;
                   $total_remainland = 0;
                  

                  foreach($sellreports as $report){
                  $total_sell_land += $report->sell_land;
                  $total_sell_land_price += $report->sell_land_price;
                  $total_sell_land_dev_cost += $report->sell_land_dev_cost;
                  $total_registry_cost += $report->registry_cost;
                  $total_others_cost += $report->others_cost;
                  $total_total_cost += $report->total_cost;
                  $total_totalRegister += $report->totalRegister;
                  //$total_totalSell += $report->totalSell;
                  $total_remainland += ($report->totalRegister-$report->sell_land);


                  ?>
               
                 <tr>
                  <td>{{$report->plot_sell_file_number}}</td>
                  <td>{{$report->plot_id_number}}</td>
                  <td>{{$report->block_name}}</td>
                  <td>{{$report->road_number}}</td>
                  <td>{{$report->plot_number}}</td>
                  <td>{{$report->face}}</td>
                  <td>{{$report->buyer_name}}</td>
                  <td>{{$report->media_name}}</td>
                  <td>{{$report->moujaname}}</td>
                  <td>{{$report->rs_dag_no}}</td>
                  <td>{{$report->saf_kobola_number}}</td>
                  <td>{{$report->totalRegister}}</td>
                  <td>{{@$report->totalSell}}</td>
                  <td>{{$report->sell_land}}</td>
                  <td>{{($report->totalRegister-$report->sell_land)}}</td>
                  <td>{{$report->sell_land_price}}</td>
                  <td>{{$report->sell_land_dev_cost}}</td>
                  <td>{{$report->registry_cost}}</td>
                  <td>{{$report->others_cost}}</td>
                  <td>{{$report->total_cost}}</td>
                  <td>{{$report->dolil_no}}</td>
                  <td>{{$report->sell_date}}</td>
                  <td></td>
                  
                 </tr>

                  <?php 
                  }
                  ?>

              </tbody>
              <tfoot>
                <tr style="color: black; font-weight: bold;">
                  <td colspan="11">Total</td>
                  <td>{{$total_totalRegister}}</td>
                  <td>{{$total_totalSell}}</td>
                  <td>{{$total_sell_land}}</td>
                  <td>{{$total_remainland}}</td>
                  <td>{{$total_sell_land_price}}</td>
                  <td>{{$total_sell_land_dev_cost}}</td>
                  <td>{{$total_registry_cost}}</td>
                  <td>{{$total_others_cost}}</td>
                  <td>{{$total_total_cost}}</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>
@endsection