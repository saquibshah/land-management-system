@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span1"></div>
      <div class="span10">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>বিক্রয় রেজিস্টার যুক্ত</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                      <th>ক্রমিক নং</th>
                      <th>প্লট আইডি নং</th>
                      <th>ব্লক নাম্বার</th>
                      <th>রোড নাম্বার</th>
                      <th>প্লট নাম্বার</th>
                      <th>ক্রেতার নাম</th>
                      <th>মৌজা</th>
                      <th>আর এস দাগ নং</th>
                      <th>সাফ কবলা ফাইল নং</th>
                      <th>বিক্রিত ভূমি </th>
                      <th>মোট  খরচ </th>
                      <th>ক্রিয়া সমূহ </th>
                  </tr>
              </thead>
              <tbody>
                <?php $i=1;?>
              @foreach($sellregisters as $sell)
                <tr class="gradeX">
                    <td>{{ $i++}}</td>
                    <td>{{ $sell-> plot_id_number}}</td>
                    <td>{{ $sell-> block_name}}</td>
                    <td>{{ $sell-> road_number}}</td>
                    <td>{{ $sell-> plot_number}}</td>
                    <td>{{ $sell-> buyer_name}}</td>
                    <td>{{ $sell-> moujaname}}</td>
                    <td>{{ $sell-> rs_dag_no}}</td>
                    <td>{{ $sell-> saf_kobola_number}}</td>
                    <td>{{ $sell-> sell_land}}</td>
                    <td>{{ $sell-> total_cost}}</td>
                    <td>
                      <a href='sellregister/edit/{{$sell->id}}' class='btn btn-xs btn-success'><i class='fa fa-edit'>পুনরায় ঠিক করুন</i></a>
                      <a href='sellregister/delete/{{$sell->id}}' class='btn btn-xs btn-danger'><i class='fa fa-trash-o' onClick='return doconfirm();'>মুছে ফেলুন</i></a>
                    </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>       
      </div>
         <div class="span1"></div>
    </div>
  </div>

<script>
  function doconfirm()
    {
        job=confirm("আপনি কি নিশ্চিত যে আপনি এই রেকর্ড মুছে দিতে চান?");
        if(job!=true)
        {
            return false;
        }
    }
</script>
@endsection