@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row vertical-offset-100">
            <div class="col-sm-6 col-sm-offset-3  col-md-5 col-md-offset-4 col-lg-4 col-lg-offset-4">
            <div class="panel panel-default">
                
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <img src="{{asset('admin/img/logonw.png')}}"  class="img-thumbnail" alt="Cinque Terre" width="304" height="236" alt="User Image">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-5 control-label">E-Mail Address</label>

                            <div class="col-md-11">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="username or e-mail" data-size="16">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-11">
                                <input id="password" type="password" class="form-control" name="password" data-size="16">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <p class="keeplogin">
                                    <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" />
                                    <label for="loginkeeping">Keep me logged in</label>
                                </p>
                        <div class="col-md-6">
                            <p class="login button">
                                <input type="submit" value="Login" class="btn btn-success" />
                            </p>
                        </div>
                            <p class="change_link">
                                <a href="">
                                <button type="button" class="btn btn-responsive botton-alignment btn-warning btn-sm">Forgot password</button>
                                </a>
                                    
                            </p>

                       <!--<div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                <button type="button" class="btn btn-responsive botton-alignment btn-warning btn-sm">Forgot password</button></a>
                            </div>
                        </div>-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
