@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>বিস্তারিত</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <center><b>Innovative Holdings Ltd.</b><br>Purbachal East Wood City<br>House # 47 (7th Floor) Road # 27,<br>
                        Block # A, Banani,
                        Dhaka- 1213, Bnagladesh.<br>
              </center>
              <thead>
                <tr  style="color: black; border: 1px solid black ">
                  <td>সাফ কবলা ফাইল নং</td>
                  <td> দাতা/দাত্রীর নাম </td>
                 
                  <td>মিডিয়ার নাম</td>
                  <td>দলিল নং</td>
                  <td>তারিখ</td>
                  <td>মৌজা</td>
                  <td>এস এ দাগ নং</td>
                  <td> আর এস দাগ নং</td>
                  <td>আর এস দাগে <br> মোট পরিমাণ (শতাংশ)</td>
                  <td>খাস জমি (শতাংশ) </td>
                  <td>অধিগ্রহণ কৃত জমি(শতাংশ)</td>
                  <td>R/S দাগে মোট ক্রয়যোগ্য জমি  (শতাংশ) </td>
                  
                  <td>ক্রয়কৃত জমির পরিমাণ (শতাংশ)</td>
                  <!--<td>সর্বমোট ক্রয়কৃত জমি</td>-->
                  <td>অবশিষ্ট জমির পরিমাণ (শতাংশ)</td>
                  <td>সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি (শতাংশ) </td>
               
                  <td>নামজারীকৃত জমি (শতাংশ)</td>
                  <td>নামজারী বিহীন জমি (শতাংশ)</td> 
                  <td> জোত নাম্বার</td>
                </tr>
              </thead>  
              <tbody>
                <?php
                $sadag_noChk = '';
                $total_rs_dag_a_poriman = 0;
                $total_kas_jomi = 0;
                $total_odigrohan_krrito_jomi = 0;
                $total_rs_dagay_total_jomi = 0;

                
                $totalPurchasedLand = 0;
                $total_remaining_land = 0;
                $total_smonnoy_extra_purchased_land = 0;
                $total_namjari_krito_land = 0;
                $total_namjari_bihin_land = 0;

                foreach($dfinals as $key => $dagrpt){

                  $dagNetJomi = ((float)$dagrpt->rs_dag_total_amount-(float)$dagrpt->kas_jomi-(float)$dagrpt->odigrohan_krrito_jomi);

                  $total_rs_dag_a_poriman += ($sadag_noChk!=$dagrpt->rs_dag_no)?$dagrpt->rs_dag_total_amount:0;
                  $total_kas_jomi += ($sadag_noChk!=$dagrpt->rs_dag_no)?$dagrpt->kas_jomi:0;
                  $total_odigrohan_krrito_jomi += ($sadag_noChk!=$dagrpt->rs_dag_no)?$dagrpt->odigrohan_krrito_jomi:0;
                  $total_rs_dagay_total_jomi += ($sadag_noChk!=$dagrpt->rs_dag_no)?$dagNetJomi:0;
                  
                  $totalPurchasedLand += $dagrpt->total_purchased_land;
                  $total_remaining_land += $dagrpt->remaining_land;
                  $total_smonnoy_extra_purchased_land += $dagrpt->smonnoy_extra_purchased_land;
                  $total_namjari_krito_land += $dagrpt->namjari_krito_land;
                  $total_namjari_bihin_land += $dagrpt->namjari_bihin_land;

                  $dagPoriman = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->rs_dag_total_amount):'';
                  $dagSorojomineJomi = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->sorojomine_jomi):'';
                  $dagKasJomi = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->kas_jomi):'';
                  $dagOdigrohan = ($sadag_noChk!=$dagrpt->rs_dag_no)?numberFormat($dagrpt->odigrohan_krrito_jomi):'';

                  $purchasedLandTxt = (isset($purchasedLand[$dagrpt->rs_dag_no]))?$purchasedLand[$dagrpt->rs_dag_no]:0;
                ?>
                <tr class="gradeX">
                  <td>{{ $dagrpt-> saf_kobola_number}}</td>
                  <td>{{ $dagrpt-> data_or_datrir_name}}</td>

                  <td>{{ $dagrpt-> name_of_media}}</td>
                  <td>{{ $dagrpt-> dolil_number}}</td>
                  <td>{{ $dagrpt-> date}}</td>
                  <td>{{ $dagrpt-> nameOfMouja}}</td>
                  <td>{{ $dagrpt-> sadag_no}}</td>
                  <td>{{ $dagrpt-> rs_dag_no}}</td>
                  <td align="right" style="text-align:right;">{{ $dagPoriman }}</td>
                  <td align="right" style="text-align:right;">{{ $dagKasJomi }}</td>
                  <td align="right" style="text-align:right;">{{ $dagOdigrohan }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($dagNetJomi-$purchasedLandTxt) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($dagrpt-> total_purchased_land) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat(($dagNetJomi-$purchasedLandTxt)-$dagrpt->total_purchased_land) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($dagrpt-> smonnoy_extra_purchased_land) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($dagrpt-> namjari_krito_land) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($dagrpt-> namjari_bihin_land) }}</td>
                  <td align="right" style="text-align:right;">{{ $dagrpt-> jooth_number}}</td>
                </tr>
                <?php
                $sadag_noChk = $dagrpt->rs_dag_no;
                @$purchasedLand[$dagrpt->rs_dag_no] += $dagrpt->total_purchased_land;
                }
                ?>
              </tbody>
              <tfoot>
                <tr style="color: black; font-weight: bold;">
                  <td colspan="8">Total</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_rs_dag_a_poriman) }} </td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_kas_jomi) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_odigrohan_krrito_jomi) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_rs_dagay_total_jomi) }}</td>
               
                  <td align="right" style="text-align:right;">{{ numberFormat($totalPurchasedLand) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_rs_dagay_total_jomi-$totalPurchasedLand) }}</td>

               
                  <td align="right" style="text-align:right;">{{ numberFormat($total_smonnoy_extra_purchased_land) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_namjari_krito_land) }}</td>
              
                  <td align="right" style="text-align:right;">{{ numberFormat($total_namjari_bihin_land) }}</td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>

@endsection