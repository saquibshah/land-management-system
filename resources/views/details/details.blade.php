@extends('layouts.master')
@section('content')


  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>বিস্তারিত</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                    <th>সাফ কবলা ফাইল নং</th>
                    <th>দলিল নং</th>
                    <th>তারিখ</th>
                    <th>দাতা/দাত্রীর নাম</th>
                    <th>মিডিয়ার নাম</th>
                    <th>মৌজা</th>
                    <th>আর এস দাগ নাম্বার</th>
                  
                    
                    <th>মোট ক্রয়কৃত জমি</th>
                    <th>সর্বমোট ক্রয়কৃত জমি</th>
                    <th>Print</th>

                </tr>
              </thead>
              <tbody>

          		@foreach($detailss as $del)
                <tr class="gradeX">
                    <td>{{ $del-> saf_kobola_number}}</td>
                    <td>{{ $del-> dolil_number}}</td>
                    <td>{{ $del-> date}}</td>
                    <td>{{ $del-> data_or_datrir_name}}</td>
                    <td>{{ $del-> name_of_media}}</td>
                    <td>{{ $del-> moujaname}}</td>
                    <td>{{ $del-> rs_dag_no}}</td>
                   
                    <td>{{ $del-> total_purchased_land}}</td>
                    <td></td>
                    <td></td>
                    
                </tr>
              @endforeach
              
              </tbody>
             

            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>


@endsection