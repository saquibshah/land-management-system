@extends('layouts.master')
@section('content')

  <div class="container-fluid">
  
    <div class="row-fluid">
  
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>মৌজা এবং আর এস দাগ অনুসারে খুজুন</h5>
          </div>
          <div class="widget-content nopadding">
          <form class="form-horizontal"  action="{{url('search/final_report')}}" method="post" accept-charset="utf-8">
            <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
           <div class="panel panel-default">

            <div  class="panel-dody container-print">

                <div class="container"><br>

                    <div class="row">
                        <div class="col-md-4  bg-border">
                            <div class="form-group">
                                <label class="control-label col-md-4 text-right">মৌজা</label>
                                <?php
                                  $all_name = DB::table('mouja')->groupBy('id')->get();
                                ?>
                                <div class="col-md-8">
                                  <select class="form-control"      name="mouja" >
                                      <option value="">মৌজার নাম</option>
                                      @foreach ($all_name as $v_name) 
                                      <option value="{{$v_name->id}}"><?php echo $v_name->nameOfMouja; ?></option>
                                       @endforeach
                                  </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4  bg-border">
                            <div class="row">
                                <!-- vat-->
                                <div class="col-md-10">
                                    <div class="form-group">
                                <label class="control-label col-md-4 text-right">আর এস দাগ নং</label>
                                <?php
                                  $all_name = DB::table('mouja_dag')->orderBy('rs_dag_no', 'ASC')->get();
                                ?>
                                <div class="col-md-8">
                                  <select class="form-control"     name="rs_dag" >
                                      <option value="">আর এস দাগ নং</option>
                                      @foreach ($all_name as $v_name) 
                                      <option value="{{$v_name->rs_dag_no}}"><?php echo $v_name->rs_dag_no; ?></option>
                                       @endforeach
                                  </select>
                                </div>
                            </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4  bg-border">
                          <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                  <label class="control-label col-md-4 text-right">সাফ কবলা ফাইল নং</label>
                                  <?php
                                    $all_name = DB::table('dag_register')->groupBy('saf_kobola_number')->get();
                                  ?>
                                <div class="col-md-8">
                                    <select class="form-control"     name="saf_kobola" >
                                      <option value="">সাফ কবলা ফাইল নং</option>
                                       @foreach ($all_name as $v_name) 
                                      <option value="{{$v_name->saf_kobola_number}}"><?php echo $v_name->saf_kobola_number; ?></option>
                                       @endforeach
                                    </select>
                                </div>
                               </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-offset-1 col-md-4  bg-border">
                            <input class="btn btn-primary"  type="submit" name="submit"  value="Search">
                        </div>

                    </div><br>
                </div>
          </div>
        </div>
      </form>
          </div>
        </div>
      </div>
    </div>
   </div>


@endsection