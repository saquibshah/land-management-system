@extends('layouts.master')
@section('content')
<div id="content">
  <div id="content-header"></div>
  <div class="container-fluid">
  
    <div class="row-fluid">
      <div class="span3"></div>
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>মৌজার বিভিন্ন তথ্য</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal"  action="{{url('moujainfo/update')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                 <input type="hidden" name="txtId" value="{{$row->id}}" class="form-control" readonly /> 
               <div class="control-group">
                <label class="control-label">মৌজার নাম</label>
                <div class="controls">
                 <?php
                  $all_name = DB::table('mouja')->get();
                ?>
                  <select name="mouja_id">
                      @foreach ($all_name as $v_name) 
                       <option value='{{$v_name->id}}' <?php if ($row->mouja_id == $v_name->id) echo ' selected="selected"';?>>{{$v_name->nameOfMouja}}</option>
                      @endforeach
                  </select>
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">এস এ দাগ নং</label>
                <div class="controls">
                  <input type="text"  placeholder="এস এ দাগ নং" value="{{$row->sa_dag_no}}"  name="sa_dag_no" required="required">
                 </div>
              </div>
             <div class="control-group">
                <label class="control-label">আর এস দাগ নং</label>
                <div class="controls">
                  <input type="text"  placeholder="আর এস দাগ নং" value="{{$row->rs_dag_no}}"  name="rs_dag_no" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">আর এস দাগে মোট জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="আর এস দাগে মোট জমি" value="{{$row->rs_dag_total_amount}}"  name="  rs_dag_total_amount" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">সরজমিনে মোট জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="সরজমিনে মোট জমি"  name="sorojomine_jomi" value="{{$row->sorojomine_jomi}}" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">খাস জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="খাস জমি" value="{{$row->kas_jomi}}"  name="kas_jomi" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">অধিগ্রহণ কৃত জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="অধিগ্রহণ কৃত জমি" value="{{$row->odigrohon_jomi}}"  name="odigrohon_jomi" required="required">
                 </div>
              </div>
              
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
                
              </div>
              </form>
          </div>
        </div>
      </div>
    </div>
    <div class="span3"></div>

    <hr>
    
</div>
</div>
</div>

@endsection