@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
           <a href="{{url('moujainfo/create')}}" class="btn btn-primary">+মৌজার বিভিন্ন তথ্য</a>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                   <th>মৌজার নাম</th>      
                   <th>এস এ দাগ নং</th>      
                   <th>আর এস দাগ নং</th>      
                   <th>আর এস দাগে মোট জমি</th>  
                   <th>সরজমিনে মোট জমি</th>     
                   <th>খাস জমি</th>            
                   <th>অধিগ্রহণ কৃত জমি</th>      
                   <th>ক্রিয়া সমূহ</th>
                </tr>
              </thead>
              <tbody>

          		@foreach($moujainfos as $mojain)
                <tr class="gradeX">
                  <td class="center">{{ $mojain-> moujaname}}</td>
                  <td class="center">{{ $mojain-> sa_dag_no}}</td>
                  <td class="center">{{ $mojain-> rs_dag_no}}</td>
                  <td class="center">{{ $mojain-> rs_dag_total_amount}}</td>
                  <td class="center">{{ $mojain-> sorojomine_jomi}}</td>
                  <td class="center">{{ $mojain-> kas_jomi}}</td>
                  <td class="center">{{ $mojain-> odigrohon_jomi}}</td>
                    
                  <td>
                      <a href='moujainfo/edit/{{$mojain->id}}' class='btn btn-xs btn-success'><i class='fa fa-edit'>পুনরায় ঠিক করুন</i></a>
                      <a href='moujainfo/delete/{{$mojain->id}}' class='btn btn-xs btn-danger'><i class='fa fa-trash-o' onClick='return doconfirm();'>মুছে ফেলুন</i></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>

<script>
  function doconfirm()
    {
        job=confirm("আপনি কি নিশ্চিত যে আপনি এই রেকর্ড মুছে দিতে চান?");
        if(job!=true)
        {
            return false;
        }
    }
</script>
@endsection