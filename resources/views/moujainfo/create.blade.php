@extends('layouts.master')
@section('content')

    <div class="row-fluid">
      <div class="span3"></div>
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>মৌজার বিভিন্ন তথ্য</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal"  action="{{url('moujainfo/create')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
               <div class="control-group">
                <label class="control-label">মৌজা</label>
                  <?php
                    $all_name = DB::table('mouja')->get();
                  ?>
                  <div class="controls">
                    <select name="mouja_id">
                        <option value="">মৌজার নাম</option>
                        @foreach ($all_name as $v_name) 
                        <option value="{{$v_name->id}}"><?php echo $v_name->nameOfMouja; ?></option>
                         @endforeach
                    </select>
                  </div>
              </div>
              <div class="control-group">
                <label class="control-label">এস এ দাগ নং</label>
                <div class="controls">
                  <input type="text"  placeholder="এস এ দাগ নং"  name="sa_dag_no" required="required">
                 </div>
              </div>
             <div class="control-group">
                <label class="control-label">আর এস দাগ নং</label>
                <div class="controls">
                  <input type="text"  placeholder="আর এস দাগ নং"  name="rs_dag_no" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">আর এস দাগে মোট জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="আর এস দাগে মোট জমি"  name="  rs_dag_total_amount" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">সরজমিনে মোট জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="সরজমিনে মোট জমি"  name="sorojomine_jomi" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">খাস জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="খাস জমি"  name="kas_jomi" required="required">
                 </div>
              </div>
              <div class="control-group">
                <label class="control-label">অধিগ্রহণ কৃত জমি</label>
                <div class="controls">
                  <input type="text"  placeholder="অধিগ্রহণ কৃত জমি"  name="odigrohon_jomi" required="required">
                 </div>
              </div>
              
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
                
              </div>
              </form>
          </div>
        </div>
      </div>
    </div>
    

@endsection