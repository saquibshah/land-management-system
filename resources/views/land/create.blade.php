@extends('layouts.master')
@section('content')
    <div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>জমির বিভিন্ন তথ্য</h5>
          </div>
          <div class="widget-content">
            <form class="form-horizontal"  action="{{url('land/create')}}" method="post" accept-charset="utf-8">
              <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
              <div class="form-group">
                <div class="col-sm-9">
                  <label>ব্লক নাম </label>
                  <input type="text" placeholder="ব্লক নাম " class="span11" name="block_name" required="required">
                </div>

                <div class="col-sm-3">
                  <label>সর্বমোট রোড </label>
                  <input type="text" placeholder="সর্বমোট রোড" class="span11" id="block_road" required="required" onkeyup="getRoad()">
                </div>
              </div>

              <div id="roadData">
                <div class="form-group">
                    <div class="col-sm-4">
                      <label>রোড নাম্বার</label>
                      <input type="text" placeholder="রোড নাম্বার" class="span11" name="road_number[]" required="required">
                    </div>
                    <div class="col-sm-5">
                      <label>ফেস</label>
                      <input type="text" placeholder="ফেস" class="span11"  name="face[]" required="required">
                    </div>
                    <div class="col-sm-3">
                      <label>সর্বমোট প্লট</label>
                      <input type="text" placeholder="সর্বমোট প্লট" class="span11" id="road_plot0" required="required" onkeyup="getPlot(0)">
                    </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    <table class="table table-bordered" id="plotData0">
                     <!--  <tr>
                        <td>প্লট নাম্বার</td>
                        <td><input type="text" placeholder="নাম্বার" class="span11" name="plot_number[0][]" required="required" style="width:60px;"></td>
                        <td><input type="text" placeholder="নাম্বার" class="span11" name="plot_number[0][]" required="required" style="width:60px;"></td>
                      </tr>
                      <tr>
                        <td>প্লট সাইজ</td>
                        <td><input type="text" placeholder="সাইজ" class="span11"  name="plot_size[0][]" required="required" style="width:60px;"></td>
                        <td><input type="text" placeholder="সাইজ" class="span11"  name="plot_size[0][]" required="required" style="width:60px;"></td>
                      </tr> -->
                    </table>
                  </div>
                </div>
              </div>
              
              <div class="form-group text-center">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('script')
<script>
  function getRoad() {
    var block_road = Number($('#block_road').val());

    var roadData = '';
    for (var i = 0; i < block_road; i++) {
      roadData += '<div class="form-group">'+
                '<div class="col-sm-4">'+
                    '<label>রোড নাম্বার</label>'+
                    '<input type="text" placeholder="রোড নাম্বার" class="span11" name="road_number[]" required="required">'+
                  '</div>'+
                  '<div class="col-sm-5">'+
                    '<label>ফেস</label>'+
                    '<input type="text" placeholder="ফেস" class="span11"  name="face[]" required="required">'+
                  '</div>'+
                  '<div class="col-sm-3">'+
                    '<label>সর্বমোট প্লট</label>'+
                    '<input type="text" placeholder="সর্বমোট প্লট" class="span11" id="road_plot'+i+'" required="required" onkeyup="getPlot('+i+')">'+
                  '</div>'+
              '</div>'+
              '<div class="form-group">'+
                '<div class="col-sm-12">'+
                  '<table class="table table-bordered" id="plotData'+i+'">'+
                    
                  '</table>'+
                '</div>'+
              '</div>';
    }
    $('#roadData').html(roadData);
  }

  function getPlot(key) {
    var road_plot = Number($('#road_plot'+key).val());

    var plotData = '<tr><td>প্লট নাম্বার</td>';
    for (var i = 0; i < road_plot; i++) {
      plotData += '<td><input type="text" placeholder="নাম্বার" class="span11" name="plot_number['+key+'][]" required="required" style="width:60px;"></td>';
    }
    plotData += '</tr>';

    plotData += '<tr><td>প্লট সাইজ</td>';
    for (var i = 0; i < road_plot; i++) {
      plotData += '<td><input type="text" placeholder="সাইজ" class="span11"  name="plot_size['+key+'][]" required="required" style="width:60px;"></td>';
    }
    plotData += '</tr>';

    $('#plotData'+key).html(plotData);
  }
</script>
@endsection