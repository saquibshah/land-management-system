@extends('layouts.master')
@section('content')
    <div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>জমির বিভিন্ন তথ্য</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal"  action="{{url('land/update')}}" method="post" accept-charset="utf-8">
                 <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                 <input type="hidden" name="txtId" value="{{$row->id}}" class="form-control" readonly /> 
               <br>
            <div class="form-group">
              <label class="control-label">ব্লক নাম </label>
              <div class="col-sm-3">
                <input type="text" placeholder="ব্লক নাম " class="span11"  name="block_name" value="{{$row->block_name}}" required="required">
              </div>

              
            </div>


            <div id="roadData">
              <div class="form-group">
                <label class="control-label">রোড নাম্বার</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="রোড নাম্বার" class="span11" name="road_number" required="required" value="{{$row->road_number}}">
                  </div>
                  <label class="control-label">ফেস</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="ফেস" class="span11"  name="face"  value="{{$row->face}}" required="required">
                  </div>
              </div>

              <div class="form-group">
                <label class="control-label">প্লট নাম্বার</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="প্লট নাম্বার" class="span11" name="plot_number" required="required" value="{{$row->plot_number}}">
                  </div>
                  <label class="control-label">প্লট সাইজ</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="প্লট সাইজ" class="span11"  name="plot_size"  value="{{$row->plot_size}}" required="required">
                  </div>
              </div>
              
              

             <div class="form-group">
              <label class="control-label"></label>
              <div class="col-sm-6">
                <table>
                  <!-- <tr>
                    <td>প্লট নাম্বার</td>
                    <td><input type="text" placeholder="নাম্বার" class="span11" name="plot_number[][]" required="required" style="width:60px;"></td>
                    <td><input type="text" placeholder="নাম্বার" class="span11" name="plot_number[][]" required="required" style="width:60px;"></td>
                  </tr>
                  <tr>
                    <td>প্লট সাইজ</td>
                    <td><input type="text" placeholder="সাইজ" class="span11"  name="plot_size[][]" required="required" style="width:60px;"></td>
                    <td><input type="text" placeholder="সাইজ" class="span11"  name="plot_size[][]" required="required" style="width:60px;"></td>
                  </tr> -->
                </table>
              </div>
                <!-- <label class="control-label">প্লট নাম্বার</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="প্লট নাম্বার" class="span11" name="plot_number" required="required">
                  </div>
                  <label class="control-label">প্লট সাইজ</label>
                  <div class="col-sm-3">
                    <input type="text" placeholder="প্লট সাইজ" class="span11"  name="plot_size" required="required">
                  </div> -->
              </div>
            </div>
             
              
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
                
              </div>
              </form>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('script')
<script>
  function getRoad() {
    $('#roadData').html();
  }
</script>
@endsection