@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
           <a href="{{url('land/create')}}" class="btn btn-primary">+জমির  বিভিন্ন তথ্য</a>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                   <th>ব্লক  নাম</th>      
                   <th>রোড নাম্বার</th>      
                   <th>ফেস</th>      
                   <th>প্লট নাম্বার</th>  
                   <th>প্লট সাইজ</th> 
                   <th>ক্রিয়া সমূহ</th>
                </tr>
              </thead>
              <tbody>

          		  @foreach($lands as $land)
                <tr class="gradeX">
                  <td class="center">{{ $land-> block_name}}</td>
                  <td class="center">{{ $land-> road_number}}</td>
                  <td class="center">{{ $land-> face}}</td>
                  <td class="center">{{ $land-> plot_number}}</td>
                  <td class="center">{{ $land-> plot_size}}</td>
                  <td>
                    <a href='land/edit/{{$land->id}}' class='btn btn-xs btn-success'><i class='fa fa-edit'>পুনরায় ঠিক করুন</i></a>
                    <a href='land/delete/{{$land->id}}' class='btn btn-xs btn-danger'><i class='fa fa-trash-o' onClick='return doconfirm();'>মুছে ফেলুন</i></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>

<script>
  function doconfirm()
    {
        job=confirm("আপনি কি নিশ্চিত যে আপনি এই রেকর্ড মুছে দিতে চান?");
        if(job!=true)
        {
            return false;
        }
    }
</script>
@endsection