@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>ক্রয় রেজিস্টার</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr style="font-size: 16px; color: black">
                  <th>ক্রমিক নং</th>      
                  <th>সাফ কবলা ফাইল নং</th>      
                  <th>দলিল নাম্বার </th>       
                  <th>তারিখ </th>
                  <th>দাতা/দাত্রীর নাম </th>
                  <th>মিডিয়ার নাম </th>
                  <th>মৌজা </th>
                  <th>আর এস দাগ নাম্বার </th>
                  <th>ক্রয়কৃত জমির পরিমাণ (শতাংশ)</th>
                  <th>বায়নায় সর্বমোট খরচ </th>
                  <th>সাফ কবলায় প্রদানকৃত টাকা</th>
                  <th>সর্বমোট খরচ</th>
                  <th>মন্তব্য</th>
                  <th>ক্রিয়া সমূহ</th>
                </tr>
              </thead>
              <tbody>
                 <?php $i=1; ?>
                @foreach($purchaseregisters as $purchase)
                <tr class="gradeX">
                    <td>{{ $i++ }}</td>
                    <td>{{$purchase -> saf_kobola_number}}</td>
                    <td>{{$purchase -> dolil_number}}</td>
                    <td>{{$purchase -> date}}</td>
                    <td>{{$purchase -> data_or_datrir_name}}</td>
                    <td>{{$purchase -> name_of_media}}</td>
                    <td>{{$purchase -> nameOfMouja}}</td>
                    <td>{{$purchase -> rs_dag_no}}</td>
                    <td>{{$purchase -> total_purchased_land}}</td>
                    <td>{{$purchase -> bina_total_cost}}</td>
                    <td>{{$purchase ->total_cost}}</td>
                    <td>{{$purchase ->sorbomot}}</td>
                    <td>{{$purchase ->montobbo}}</td>
                    <td>
                      <a href='purchaseregister/edit/{{$purchase->purchaseId}}' class='btn btn-xs btn-success'><i class='fa fa-edit'></i></a>
                      <a href='purchaseregister/delete/{{$purchase->purchaseId}}' class='btn btn-xs btn-danger'><i class='fa fa-trash-o' onClick='return doconfirm();'></i></a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>

<script>
  function doconfirm()
    {
        job=confirm("আপনি কি নিশ্চিত যে আপনি এই রেকর্ড মুছে দিতে চান?");
        if(job!=true)
        {
            return false;
        }
    }
</script>
@endsection