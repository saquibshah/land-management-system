<!DOCTYPE html>
<html lang="en">
<head>
<title>Innovative Land Management System</title>
<meta charset="UTF-8" />
  <title></title>

  <style media="print">
  .printHide { display: none;}
  </style>
</head>
<body>
  <button onclick="window.print()" value="Print" title="Print" class="no-print">Print</button>
  <center>
    <table>
      <tr>
        <td style="text-align: center;">ক্রয় রেজিস্টার</td>
      </tr>
      <tr>
        <td style="text-align: center;">Innovative Holdings Ltd. (Purbachal East Wood City)</td>
      </tr>
      <tr>
        <td style="text-align: center;">House # 47 (7th Floor) Road # 27, Block # A, Banani, Dhaka- 1213.</td>
      </tr>
    </table> 
  </center>         
  <table border="1" cellpadding="0" cellspacing="0">
    <thead>
      <tr style="color: black; border: 1px solid black">
        <td>সাফ কবলা ফাইল নং</td>
        <td>আর এস দাগ নং</td>
        <td>বায়নাকৃত জমির পরিমাণ</td>
        <td>বায়না বাজেট নং</td>

        <td>বায়না বাজেটকৃত মোট টাকা</td>
        <td>দাতাকে প্রদানকৃত টাকা</td>
        <td>বায়না রেজিস্ট্রেশন খরচ</td>
        <td>বায়না মিডিয়া কমিশন</td>

        <td>বায়না আনুষঙ্গিক খরচ</td>
        <td>বায়না সর্বমোট খরচ</td>
        <td>বায়না ফেরতকৃত টাকা</td>
        <td>সাফ কবলা বাজেট নং</td>

        <td>সাফকবলা  বাজেটকৃত মোট টাকা</td>
        <td>সাবকবলায়  দাতাকে প্রদানকৃত টাকা</td>
        <td>দলিলে প্রদর্শিত মূল্য</td>
        <td>সাবকবলায় রেজিস্ট্রেশন খরচ</td>

        <td>সাবকবলায় মিডিয়া কমিশন</td>
        <td>সাবকবলায় আনুষঙ্গিক খরচ</td>
        <td>সাফ কবলায় মোট খরচ</td>
        <td>সাবকবলায় ফেরতকৃত টাকা (যদি থাকে) </td>

        <td>সর্বমোট খরচ</td>
        <td>মন্তব্য </td>
      </tr>
    </thead>
    <tbody>
  		<?php 
      $safKobolaCk = '';
      $totalPurchasedLand = 0;
      $total_bina_total_budget_money = 0;
      $total_bina_datak_prodan = 0;
      $total_bina_registration_cost = 0;
      $total_bina_meadia_commision = 0;

      $total_bina_anosongik_cost = 0;
      $total_bina_total_cost = 0;
      $total_bina_returned_money = 0;
      $total_total_budget_money = 0;
      $total_mentioned_price_in_dolil = 0;
      $total_registration_cost = 0;
      $total_meadia_commision = 0;
      $total_anosongik_cost = 0;
      $total_total_cost = 0;
      $total_returned_money = 0;
      $total_sorbomot = 0;
      $total_price_purchased_land = 0;

      foreach($purchaseas as $purchaserpt){
        $totalPurchasedLand += $purchaserpt->total_purchased_land;

        $safKobolaNo = ($safKobolaCk!=$purchaserpt->saf_kobola_number)?$purchaserpt->saf_kobola_number:'"';


        //N+P+Q+R == S
        $total_cost = ($purchaserpt->toatal_price_of_purchased_land+$purchaserpt->registration_cost+$purchaserpt->meadia_commision+$purchaserpt->anosongik_cost);

        //J+S == U
        $sorbomot = ($purchaserpt->bina_total_cost+$total_cost);

        //M-S == U
        $returned_money = ($purchaserpt->total_budget_money-$total_cost);
      ?>
      <tr class="gradeX">
        <td align="center" title="A">{{ $safKobolaNo}}</td>

        <td align="center" title="B">{{ $purchaserpt->rs_dag_no}}</td>
        <td align="right" title="C">{{ numberFormat($purchaserpt->total_purchased_land)}}</td>
        <?php
        if($safKobolaCk!=$purchaserpt->saf_kobola_number)
        {
          $total_bina_total_budget_money += $purchaserpt->bina_total_budget_money;
          $total_bina_datak_prodan += $purchaserpt->bina_datak_prodan;
          $total_bina_registration_cost += $purchaserpt->bina_registration_cost;
          $total_bina_meadia_commision += $purchaserpt->bina_meadia_commision;

          $total_bina_anosongik_cost += $purchaserpt->bina_anosongik_cost;
          $total_bina_total_cost     += $purchaserpt->bina_total_cost;
          $total_bina_returned_money += $purchaserpt->bina_returned_money;
          $total_total_budget_money  += $purchaserpt->total_budget_money;
          $total_registration_cost   += $purchaserpt->registration_cost;
          $total_meadia_commision    += $purchaserpt->meadia_commision;
          $total_anosongik_cost      += $purchaserpt->anosongik_cost;
          //$total_total_cost          += $purchaserpt->total_cost;
          $total_total_cost          += $total_cost;
          //$total_returned_money      += $purchaserpt->returned_money;
          $total_returned_money      += $returned_money;
          //$total_sorbomot            += $purchaserpt->sorbomot;
          $total_sorbomot            += $sorbomot;
          $total_price_purchased_land+= $purchaserpt->toatal_price_of_purchased_land
        ?>
        <td align="center" title="D">{{ $purchaserpt->bina_budget_number }}</td>

        <td align="right" title="E">{{ numberFormat($purchaserpt->bina_total_budget_money)}}</td>
        <td align="right" title="F">{{ numberFormat($purchaserpt->bina_datak_prodan)}}</td>
        <td align="right" title="G">{{ numberFormat($purchaserpt->bina_registration_cost)}}</td>
        <td align="right" title="H">{{ numberFormat($purchaserpt->bina_meadia_commision)}}</td>

        <td align="right" title="I">{{ numberFormat($purchaserpt->bina_anosongik_cost)}}</td>
        <td align="right" title="J">{{ numberFormat($purchaserpt->bina_total_cost)}}</td>
        <td align="right" title="K">{{ numberFormat($purchaserpt->bina_returned_money)}}</td>
        <td align="center" title="L">{{ $purchaserpt->budget_number}}</td>

        <td align="right" title="M">{{ numberFormat($purchaserpt->total_budget_money)}}</td>
        <td align="right" title="N">{{ numberFormat($purchaserpt->toatal_price_of_purchased_land)}}</td>
        <td align="right" title="O">{{ numberFormat($purchaserpt->mentioned_price_in_dolil)}}</td>
        <td align="right" title="P">{{ numberFormat($purchaserpt->registration_cost)}}</td>

        <td align="right" title="Q">{{ numberFormat($purchaserpt->meadia_commision)}}</td>
        <td align="right" title="R">{{ numberFormat($purchaserpt->anosongik_cost)}}</td>
        <td align="right" title="S">{{ numberFormat($total_cost)}}</td>
        <td align="right" title="T">{{ numberFormat($returned_money)}}</td>

        <td align="right" title="U">{{ numberFormat($sorbomot)}}</td>
        <td></td>
        <?php
        }
        else{
          echo '<td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>
          <td align="center">"</td>';
        }
        ?>
      </tr>
      <?php
        $safKobolaCk = $purchaserpt->saf_kobola_number;
      }
      ?>
      
      <tr style="color: #000; font-weight: bold; border: 1px solid #000">
        <td colspan="2">Total</td>
        <td>{{ numberFormat($totalPurchasedLand )}}</td>
        <td>&nbsp;</td>
        <td align="right">{{ numberFormat($total_bina_total_budget_money )}}</td>
        <td align="right">{{ numberFormat($total_bina_datak_prodan )}}</td>
        <td align="right">{{ numberFormat($total_bina_registration_cost )}}</td>
        <td align="right">{{ numberFormat($total_bina_meadia_commision )}}</td>

        <td align="right">{{ numberFormat($total_bina_anosongik_cost )}}</td>
        <td align="right">{{ numberFormat($total_bina_total_cost )}}</td>
        <td align="right">{{ numberFormat($total_bina_returned_money )}}</td>
        <td></td>
        <td align="right">{{ numberFormat($total_total_budget_money )}}</td>
        <td align="right">{{ numberFormat($total_price_purchased_land )}}</td>
        <td></td>
        <td align="right">{{ numberFormat($total_registration_cost )}}</td>
        <td align="right">{{ numberFormat($total_meadia_commision )}}</td>
        <td align="right">{{ numberFormat($total_anosongik_cost )}}</td>
        <td align="right">{{ numberFormat($total_total_cost )}}</td>
        <td align="right">{{ numberFormat($total_returned_money )}}</td>
        <td align="right">{{ numberFormat($total_sorbomot )}}</td>
        <td></td>
      </tr>
      
      <tr style="color: black; border: 1px solid black" class="printHide">
        <td>সাফ কবলা ফাইল নং</td>
        <td>আর এস দাগ নং</td>
        <td>বায়নাকৃত জমির পরিমাণ</td>
        <td>বায়না বাজেট নং</td>

        <td>বায়না বাজেটকৃত মোট টাকা</td>
        <td>দাতাকে প্রদানকৃত টাকা</td>
        <td>বায়না রেজিস্ট্রেশন খরচ</td>
        <td>বায়না মিডিয়া কমিশন</td>

        <td>বায়না আনুষঙ্গিক খরচ</td>
        <td>বায়না সর্বমোট খরচ</td>
        <td>বায়না ফেরতকৃত টাকা</td>
        <td>সাফ কবলা বাজেট নং</td>

        <td>সাফকবলা  বাজেটকৃত মোট টাকা</td>
        <td>সাবকবলায়  দাতাকে প্রদানকৃত টাকা</td>
        <td>দলিলে প্রদর্শিত মূল্য</td>
        <td>সাবকবলায় রেজিস্ট্রেশন খরচ</td>

        <td>সাবকবলায় মিডিয়া কমিশন</td>
        <td>সাবকবলায় আনুষঙ্গিক খরচ</td>
        <td>সাফ কবলায় মোট খরচ</td>
        <td>সাবকবলায় ফেরতকৃত টাকা (যদি থাকে) </td>

        <td>সর্বমোট খরচ</td>
        <td>মন্তব্য </td>
      </tr>
    </tbody>
  </table>



</body>
</html>