@extends('layouts.master')
@section('content')
<div class="row-fluid">
      <div class="span2"></div>
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>ক্রয় রেজিস্টার</h5>
          </div>
    <div class="widget-content nopadding"><br>
            <form action="{{url('purchaseregister/update')}}" method="post" class="form-horizontal">
                <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                  <input type="hidden" name="txtId" value="{{$row->id}}" class="form-control" readonly />
                  <div class="form-group">
                <label class="control-label">সাফ কবলা ফাইল নং</label>
                <div class="col-sm-3">
                 <?php
                  //$all_name = DB::table('dag_register')->distinct('saf_kobola_number')->get();
                  $all_name = DB::table('dag_register')
                          ->select('saf_kobola_number')
                          ->groupBy('saf_kobola_number')
                          ->get();
                ?>
                  <select name="saf_kobola_number"  required="required" id="saf_kobola_number" name="saf_kobola_number"  class="selectpicker" data-live-search="true">
                  <option value="">সাফ কবলা ফাইল নং</option>
                      @foreach ($all_name as $v_name) 

                        <option value="{{$v_name->saf_kobola_number}}" <?php echo ($row->saf_kobola_number==$v_name->saf_kobola_number)?'selected':'';?>><?php echo $v_name->saf_kobola_number; ?></option>
                      @endforeach
                  </select>
                 </div>
                 
                </div>
                <div class="form-group">
                  <!-- <label class="control-label">আর এস দাগ নং</label> -->
                  <div class="col-sm-3"></div>
                  <div class="col-sm-6">
                    <table  class="table table-bordered"  id="korida_onso" name="korida_onso">
                     
                      @foreach ($saf_kobola as $saf)
                      <tr>
                        <td> আর এস দাগ নং : {{$saf->rs_dag_no}}</td>
                        <td> খরিদা অংশ : {{$saf->total_purchased_land}}</td>
                      </tr>
                      @endforeach
                    </table>
                  </div> 
          
               
                <div class="form-group"></div>
              <div class="panel-heading">
                  <p style="text-align:center; color: #0033cc; font-size: 16px; font-weight: bold;">বায়না</p>
              </div>
                  <hr style="border: 1px solid #000000">
              <div class="form-group">
                <label class="control-label">বায়না বাজেট নং :</label>
                <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="বায়না বাজেট নং" id="bina_budget_number" class="form-control"  name="bina_budget_number" required="required" value="{{$row->bina_budget_number}}" />
                </div>
                <label class="control-label">বায়না বাজেটকৃত মোট টাকা :</label>
                 <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="বায়না বাজেটকৃত মোট টাকা" id="bina_total_budget_money" class="form-control"  name="bina_total_budget_money" required="required" value="{{$row->bina_total_budget_money}}" onkeyup="calculate(this.id)" />
                </div>
              </div>
              
              <div class="form-group">
                <label class="control-label">দাতাকে প্রদান</label>
                 <div class="col-sm-3">
                  <input type="text"  class="span11" placeholder="দাতাকে প্রদান" id="bina_datak_prodan"  name="bina_datak_prodan" class="form-control" required="required"  onkeyup="calculate(this.id)"  value="{{$row->bina_datak_prodan}}" />
                </div>
                <label class="control-label">বায়না রেজিস্ট্রেশন খরচ:</label>
                 <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="বায়না রেজিস্ট্রেশন খরচ" id="bina_registration_cost" name="bina_registration_cost"  onkeyup="calculate(this.id)" class="form-control" required="required" value="{{$row->bina_registration_cost}}" />
                </div>
              </div>
              
              <div class="form-group">
                <label class="control-label">বায়না মিডিয়া কমিশন:</label>
                 <div class="col-sm-3">
                  <input type="text" class="span11"  onkeyup="calculate(this.id)" placeholder="বায়না মিডিয়া কমিশন" id="bina_meadia_commision" name="bina_meadia_commision" class="form-control" required="required" value="{{$row->bina_meadia_commision}}" />
                </div>
                 <label class="control-label">বায়না আনুষঙ্গিক খরচ:</label>
             <div class="col-sm-3">
                  <input type="text" class="span11"  onkeyup="calculate(this.id)" placeholder="বায়না আনুষঙ্গিক খরচ" 
                   id="bina_anosongik_cost"  name="bina_anosongik_cost" class="form-control" value="{{$row->bina_anosongik_cost}}" required="required" />
                </div>
              </div>
             
               
      
            <div class="form-group">
                <label class="control-label">বায়না সর্বমোট খরচ:</label>
           <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="বায়না সর্বমোট খরচ" readonly name="bina_total_cost" value="{{$row->bina_total_cost}}" id="bina_total_cost" />
                </div>
                <label class="control-label">বায়না ফেরতকৃত টাকা(যদি থাকে):</label>
          <div class="col-sm-3">
                  <input type="text" class="span11"  readonly placeholder="বায়না ফেরতকৃত টাকা" value="{{$row->bina_returned_money}}" name="bina_returned_money" id="bina_returned_money" />
                </div>
              </div>
              
              <div class="panel-heading">
                   <p style="text-align:center; color: #0033cc; font-size: 16px; font-weight: bold;"> সাফ কবলা বাজেট</p>
              </div>
               <hr style="border: 1px solid #000000">
              <div class="form-group">
                <label class="control-label">সাফ কবলা বাজেট নং:</label>
                 <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="বাজেট নং" id="budget_number" name="budget_number" value="{{$row->budget_number}}" required="required" />
                </div>
                <label class="control-label">বাজেটকৃত মোট টাকা:</label>
                  <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="বাজেটকৃত মোট টাকা" id="total_budget_money" value="{{$row->total_budget_money}}" name="total_budget_money" required="required" onkeyup="sab_calculate(this.id)" />
                </div>
              </div>
             
            <div class="form-group">
                <label class="control-label">ক্রয়কৃত ভূমির মোট মূল্য:</label>
              <div class="col-sm-3">
                  <input type="text" class="span11"   placeholder="ক্রয়কৃত ভূমির মোট মূল্য" value="{{$row->toatal_price_of_purchased_land}}" name="toatal_price_of_purchased_land" id="toatal_price_of_purchased_land" onkeyup="sab_calculate(this.id)" required="required" />
                </div>
                <label class="control-label">দলিলে প্রদর্শিত মূল্য:</label>
             <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="দলিলে প্রদর্শিত মূল্য" value="{{$row->mentioned_price_in_dolil}}" id="mentioned_price_in_dolil" name="mentioned_price_in_dolil" required="required" />
                </div>
              </div>
             
         <div class="form-group">
                <label class="control-label">পে-অর্ডার মূল্য:</label>
                  <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="পে-অর্ডার মূল্য"  value="{{$row->pe_order_mullo}}" name="pe_order_mullo" required="required" />
                </div>
                <label class="control-label">রেজিস্ট্রেশন খরচ:</label>
         <div class="col-sm-3">
                  <input type="text" class="span11"  placeholder="রেজিস্ট্রেশন খরচ" onkeyup="sab_calculate(this.id)" id="registration_cost" value="{{$row->registration_cost}}" name="registration_cost" required="required" />
                </div>
              </div>
             
         <div class="form-group">
                <label class="control-label">মিডিয়া কমিশন:</label>
                  <div class="col-sm-3">
                  <input type="text" class="span11" onkeyup="sab_calculate(this.id)"   placeholder="মিডিয়া কমিশন" id="meadia_commision" value="{{$row->meadia_commision}}" name="meadia_commision" required="required" />
                </div>
                <label class="control-label">আনুষঙ্গিক খরচ:</label>
                <div class="col-sm-3">
                  <input type="text" class="span11" onkeyup="sab_calculate(this.id)" id="anosongik_cost" value="{{$row->anosongik_cost}}"  placeholder="আনুষঙ্গিক খরচ" name="anosongik_cost" required="required" />
                </div>
              </div>
              
               
              <div class="form-group">
                <label class="control-label">সাফ কবলায় মোট খরচ:</label>
                   <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="মোট খরচ" id="total_cost" value="{{$row->total_cost}}" readonly name="total_cost" readonly  />
                </div>
                <label class="control-label">ফেরতকৃত টাকা(যদি থাকে):</label>
            <div class="col-sm-3">
                  <input type="text" class="span11" placeholder="ফেরতকৃত টাকা" readonly name="returned_money" id="returned_money" value="{{$row->returned_money}}" />
                </div>
              </div>
               
           <div class="form-group">
                <label class="control-label">সর্বমোট খরচ:</label>
                <div class="col-sm-9">
                  <input type="text" class="span11" readonly placeholder="সর্বমোট খরচ" value="{{$row->sorbomot}}" id="sorbomot" name="sorbomot"  />
                </div>
              </div>
                 <div class="form-group">
                <label class="control-label">মন্তব্য</label>
               <div class="col-sm-9">
                  <textarea class="span11" required="required"  name="montobbo" class="form-control" size="131">{{$row->montobbo}}</textarea>
               </div>
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success">সংরক্ষণ করুন</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    
@endsection