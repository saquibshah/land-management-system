@extends('layouts.master')
@section('content')
<div class="container-fluid no-print">
   <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-align-justify"></i></span>
            <h5>সর্বমোট খরিদা সম্পত্তি</h5>
          </div>
          <div class="widget-content nopadding">
          <form class="form-horizontal"  action="{{url('purchase_report')}}" method="post" accept-charset="utf-8">
            <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
                 <div class="col-md-4 bg-border">
                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                         <label class="control-label">মৌজা</label>
                                <?php
                                  $all_name = DB::table('mouja')->get();
                                ?>
                                <div class="controls">
                                  <select  name="mouja" class="span11" >
                                      <option value="">মৌজার নাম</option>
                                      @foreach ($all_name as $v_name) 
                                      <option value="{{$v_name->id}}"><?php echo $v_name->nameOfMouja; ?></option>
                                       @endforeach
                                  </select>
                                </div>

                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4  bg-border">
                            <div class="row">
                               <div class="col-md-10">
                                 <div class="form-group">
                                   <label class="control-label">Date </label>
                                     <div class="controls">
                                       <div class="input-append date datepicker">
                                        <input type="date" placeholder="Form" name="fromDate"  data-date-format="mm/dd/yyyy" class="span11" >
                                       </div>
                                      </div>
                                 </div>

                                </div>
                            </div>
                    </div>
                   <div class="col-md-4  bg-border">
                    <div class="row">
                       <div class="col-md-10">
                         <div class="form-group">
                           <label class="control-label">Date </label>
                             <div class="controls">
                               <div class="input-append date datepicker">
                               <input type="date" placeholder="to" name="toDate"  data-date-format="mm/dd/yyyy" class="span11" >
                                </div>
                              </div>
                         </div>
                      </div>
                    </div>
                  </div>
                <div class="col-md-3"> 
                  <input type="submit"  class="btn btn-primary" value="Submit">
                </div>
         </form>
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
         <button onclick="window.print()" value="Print" title="Print" type="hidden" class="no-print" >Print</button>
        <div class="widget-box">
          <div class="widget-title">
             <span class="icon"><i class="icon-th"></i></span> 
            <h5>সর্ব মোট কোম্পানির খরিদা ভূমি</h5>
          </div>
          <div class="widget-content nopadding">
            <center>
              <table>
                <tr>
                  <td style="text-align: center;">সর্ব মোট কোম্পানির খরিদা ভূমি</td>
                </tr>
                <tr>
                  <td style="text-align: center;">Innovative Holdings Ltd. (Purbachal East Wood City)</td>
                </tr>
                <tr>
                  <td style="text-align: center;">House # 47 (7th Floor) Road # 27, Block # A, Banani, Dhaka- 1213.</td>
                </tr>
              </table> 
            </center>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td>সাফ কবলা ফাইল নং</td>
                  <td>দাতা/দাত্রীর নাম</td>
                  <td>মিডিয়ার নাম</td>
                  <td>দলিল নং</td>
                  <td>তারিখ</td>
                  <td>মৌজা</td>
                  <td>R/S দাগ নং</td>
                  <td>ক্রয়কৃত ভূমি </td>
                  <td style="width:120px;">সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি (শতাংশ) </td>
                </tr>
              </thead>
              <tbody>
                <?php 
                $safKobolaNoChk = '';
                $total_total_purchased_land = 0;
                $total_smonnoy_extra_purchased_land = 0;

                foreach($totalpurchases as $total){
                $total_total_purchased_land += $total->total_purchased_land;
                $total_smonnoy_extra_purchased_land += $total->smonnoy_extra_purchased_land;
                ?>
                <tr class="gradeX">
                    <td>{{ ($safKobolaNoChk!=$total->saf_kobola_number)?$total->saf_kobola_number:'"'}}</td>
                    <td>{{ ($safKobolaNoChk!=$total->saf_kobola_number)?$total->data_or_datrir_name:'"'}}</td>
                    <td>{{ ($safKobolaNoChk!=$total->saf_kobola_number)?$total->name_of_media:'"'}}</td>
                    <td>{{ ($safKobolaNoChk!=$total->saf_kobola_number)?$total->dolil_number:'"'}}</td>
                    <td>{{ ($safKobolaNoChk!=$total->saf_kobola_number)?dateFormat($total->date):'"'}}</td>
                    <td>{{ ($safKobolaNoChk!=$total->saf_kobola_number)?$total->moujaname:'"'}}</td>
                    <td>{{ $total-> rs_dag_no}}</td>
                    <td align="right" style="text-align:right;">{{ numberFormat($total-> total_purchased_land)}}</td>
                    <td align="right" style="text-align:right;">{{ numberFormat($total->smonnoy_extra_purchased_land)}}</td>
                </tr>
                <?php
                $safKobolaNoChk = $total->saf_kobola_number;
                }
                ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="7">মোট শতাংশ</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_total_purchased_land) }}</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_smonnoy_extra_purchased_land) }}</td>
                </tr>
                <tr>
                  <td colspan="7">মোট বিঘা</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_total_purchased_land/30) }}</td>
                  <td align="right" style="text-align:right;"></td>
                </tr>
                <tr>
                  <td colspan="7">মোট একর</td>
                  <td align="right" style="text-align:right;">{{ numberFormat($total_total_purchased_land/100) }}</td>
                  <td align="right" style="text-align:right;"></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>       
      </div>
    </div>
  </div>
@endsection