@extends('layouts.master')
@section('content')
<div class="container-fluid no-print">
   <div class="row-fluid">
      <div class="span12">
        <form class="form-horizontal"  action="{{url('total')}}" method="post" accept-charset="utf-8">
            <input type="hidden" name="_token" value="<?php echo csrf_token()?>" />
               <div class="col-md-4 bg-border">
                  <div class="row">
                    <div class="col-md-10">
                      <div class="form-group">
                         <label class="control-label">মৌজা</label>
                                <?php
                                  $all_name = DB::table('mouja')->get();
                                ?>
                                <div class="controls">
                                  <select  name="mouja" class="span11" >
                                      <option value="">মৌজার নাম</option>
                                      @foreach ($all_name as $v_name) 
                                      <option value="{{$v_name->id}}"><?php echo $v_name->nameOfMouja; ?></option>
                                       @endforeach
                                  </select>
                                </div>

                            </div>
                        </div>
                      </div>
                    </div>
                      <div class="col-md-4  bg-border">
                            <div class="row">
                               <div class="col-md-4">
                                 <div class="form-group">
                                   <label class="control-label">Date </label>
                                     <div class="controls">
                                       <div class="input-append date datepicker">
                                       <input type="date" placeholder="Form" name="fromDate"  data-date-format="mm/dd/yyyy" class="span11" >
                                        </div>
                                       </div>
                                 </div>

                                </div>
                            </div>
                        </div>
                   <div class="col-md-4  bg-border">
                    <div class="row">
                       <div class="col-md-4">
                         <div class="form-group">
                           <label class="control-label">Date </label>
                             <div class="controls">
                               <div class="input-append date datepicker">
                               <input type="date" placeholder="to" name="toDate"  data-date-format="mm/dd/yyyy" class="span11" >
                                </div>
                              </div>
                         </div>
                      </div>
                    </div>
                  </div><br>
                <div class="col-md-3"> 
                  <input type="submit"  class="btn btn-primary" value="Submit">
                </div>
             </form>
           </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
      <button onclick="window.print()" value="Print" title="Print" type="hidden" class="no-print" >Print</button>
      <div class="widget-box">
        <div class="widget-title">
           <span class="icon"><i class="icon-th"></i></span> 
          <h5>সর্বমোট প্রকল্প ব্যায় প্রতিবেদন </h5>
        </div>
        <div class="widget-content nopadding">
          <center>
            <table>
              <tr>
                <td style="text-align: center;">সর্বমোট প্রকল্প ব্যায়</td>
              </tr>
              <tr>
                <td style="text-align: center;">Innovative Holdings Ltd. (Purbachal East Wood City)</td>
              </tr>
              <tr>
                <td style="text-align: center;">House # 47 (7th Floor) Road # 27, Block # A, Banani, Dhaka- 1213.</td>
              </tr>
            </table> 
          </center>
          <table class="table table-bordered data-table">
            <thead>
              <tr style="font-size: 16px; color: black">
                <td>ক্রমিক নং</td>
                <td>সাফ কবলা নাম্বার</td>
                <td>তারিখ</td>
                <td>আর এস দাগ নং</td>
                <td>জমির পরিমাণ</td>
                <td>দলিলে প্রদর্শিত মূল্য</td>
               <!--  <td>পে-অর্ডার মূল্য</td> -->
                <td>জমির ক্রয়কৃত মূল্য</td>
                <td>রেজিস্ট্রেশন খরচ</td>
                <td>মিডিয়া কমিশন</td>
                <td>আনুসংগিক খরচ</td>
                <td>মোট খরচ</td>
                <td>মন্তব্য</td>
              </tr>
            </thead>
         
            <tbody>
              <?php 
              $i=1; 
              $safKobolaCk = '';
              $total_total_purchased_land = 0;
              $total_mentioned_price_in_dolil = 0;
              $total_pe_order_mullo = 0;
              $total_toatal_price_of_purchased_land = 0;

              $total_final_registration_cost = 0;
              $total_final_meadia_commision = 0;
              $total_final_anosongik_cost = 0;
              $total_final_sorbomot = 0;
              foreach($costs as $purchase){
                $total_total_purchased_land += $purchase->total_purchased_land;
                
              ?>
              <tr class="gradeX">
                <?php
                  if($safKobolaCk!=$purchase->saf_kobola_number)
                  {
                  ?>
                    <td title="A"><?php echo  $i++ ;?></td>
                    <td title="B"><?php echo $purchase->saf_kobola_number;?></td>
                    <td title="C"><?php echo dateFormat($purchase->date);?></td>
                  <?php
                  }
                  else{
                    echo '<td align="center" title="A">"</td>
                    <td align="center" title="B">"</td>
                    <td align="center" title="C">"</td>';
                  }
                  ?>

                  <td title="D"><?php echo $purchase->rs_dag_no;?></td>
                  <td title="E"><?php echo $purchase->total_purchased_land;?></td>

                  <?php
                  if($safKobolaCk!=$purchase->saf_kobola_number)
                  {
                    $total_mentioned_price_in_dolil += $purchase->mentioned_price_in_dolil;
                    /*$total_pe_order_mullo += $purchase->pe_order_mullo;*/

                    $purchased_land = ($purchase->bina_datak_prodan+$purchase->toatal_price_of_purchased_land);
                    $total_registration_cost = ($purchase->registration_cost+$purchase->bina_registration_cost);
                    $total_meadia_commision = ($purchase->meadia_commision+$purchase->bina_meadia_commision);
                    $total_anosongik_cost = ($purchase->anosongik_cost+$purchase->bina_anosongik_cost);

                    $sorbomot = ($purchased_land+$total_registration_cost+$total_meadia_commision+$total_anosongik_cost);

                    $total_toatal_price_of_purchased_land += $purchased_land;
                    $total_final_registration_cost += $total_registration_cost ;
                    $total_final_meadia_commision += $total_meadia_commision;
                    $total_final_anosongik_cost += $total_anosongik_cost;
                    $total_final_sorbomot += $sorbomot;
                  ?>
                    <td title="F" align="right" style="text-align: right;"><?php echo numberFormat($purchase->mentioned_price_in_dolil);?></td>
                    <!-- <td title="G" align="right" style="text-align: right;"><?php echo numberFormat($purchase->pe_order_mullo);?></td> -->
                    <td title="H" align="right" style="text-align: right;"><?php echo numberFormat($purchased_land);?></td>
                    <td title="I" align="right" style="text-align: right;"><?php echo numberFormat($total_registration_cost);?></td>
                    <td title="J" align="right" style="text-align: right;"><?php echo numberFormat($total_meadia_commision);?></td>
                    <td title="K" align="right" style="text-align: right;"><?php echo numberFormat($total_anosongik_cost);?></td>
                    <td title="L" align="right" style="text-align: right;"><?php echo numberFormat($sorbomot);?></td>
                    <td title="M" align="right" style="text-align: right;"></td> 
                  <?php
                  }
                  else{
                    echo '<td align="center" title="F">"</td>
                    <td align="center" title="G">"</td>
                    <td align="center" title="H">"</td>
                    <td align="center" title="I">"</td>
                    <td align="center" title="J">"</td>
                    <td align="center" title="K">"</td>
                    <td align="center" title="L">"</td>
                    <td title="M"></td>';
                  }
                  ?>
              </tr>
              <?php
                $safKobolaCk = $purchase->saf_kobola_number;
              }
              ?>
            </tbody>
            <tfoot>
              <tr style="color: #000">
                <td colspan="4"> Total : </td>
                <td align="right" style="text-align: right;">{{ numberFormat($total_total_purchased_land) }}</td>

                <td align="right" style="text-align: right;">{{ numberFormat($total_mentioned_price_in_dolil) }}</td>
                <!-- <td align="right" style="text-align: right;">{{ numberFormat($total_pe_order_mullo) }}</td> -->
                <td align="right" style="text-align: right;">{{ numberFormat($total_toatal_price_of_purchased_land) }}</td>
                <td align="right" style="text-align: right;">{{ numberFormat($total_final_registration_cost) }}</td>
                <td align="right" style="text-align: right;">{{ numberFormat($total_final_meadia_commision) }}</td>
                <td align="right" style="text-align: right;">{{ numberFormat($total_final_anosongik_cost) }}</td>
                <td align="right" style="text-align: right;">{{ numberFormat($total_final_sorbomot) }}</td>
                <td align="right" style="text-align: right;"></td>
              </tr>

              <tr style="font-size: 16px; color: black">
                <td>ক্রমিক নং</td>
                <td>সাফ কবলা নাম্বার</td>
                <td>তারিখ</td>
                <td>আর এস দাগ নং</td>
                <td>জমির পরিমাণ</td>
                <td>দলিলে প্রদর্শিত মূল্য</td>
                <!-- <td>পে-অর্ডার মূল্য</td> -->
                <td>জমির ক্রয়কৃত মূল্য</td>
                <td>রেজিস্ট্রেশন খরচ</td>
                <td>মিডিয়া কমিশন</td>
                <td>আনুসংগিক খরচ</td>
                <td>মোট খরচ</td>
                <td>মন্তব্য</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>       
    </div>
  </div>
</div>
@endsection