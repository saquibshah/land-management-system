@extends('layouts.master')
@section('content')
<div class="container-fluid"><br>
    <div class="container">
        <div class="row">
            <div class="col-md-12"> 
                <button onclick="window.print()" value="Print" title="Print" >Print</button>
                <!-- <a href="{{url('/alltoexcel')}}" class="btn btn-primary"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a> -->
            </div>
        </div>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title">
             <!--<span class="icon"><i class="icon-th"></i></span> -->
            <h5></h5><br>
             
          </div>
          <center><b>Innovative Holdings Ltd.</b><br>Purbachal East Wood City<br>House # 47 (7th Floor) Road # 27,<br>
                        Block # A, Banani,
                        Dhaka- 1213, Bnagladesh.<br></center>
                           <hr style="border: 1px solid #000000">

          <div class="widget-content nopadding">
            <table  border="1" cellpadding="0" cellspacing="0">
                <thead>   
                  <tr style="color: black">
                        <td>সাফ কবলা ফাইল নং</td>
                        <td>দাতা/দাত্রীর নাম</td>
                        <td>মিডিয়ার নাম</td>
                        <td>দলিল নাম্বার</td>
                        <td>তারিখ</td>
                        <th>মৌজা</th>
                        <td>এস এ  দাগ নাম্বার</td>
                        <td>আর এস দাগ নাম্বার</td>
                        <td>আর এস দাগে জমির পরিমাণ (শতাংশ)</td>
                        <td>খাস জমি  (শতাংশ)</td>
                        <td>অধিগ্রহণ কৃত জমি</td>
                        <td>আর এস দাগে মোট ক্রয়যোগ্য জমি</td>
                        <td>ক্রয়কৃত জমি</td>
                        <td>অবশিষ্ট জমি </td>      
                        <td>সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি (শতাংশ)</td>
                        <td>নামজারীকৃত জমি (শতাংশ)</td>
                        <td>নামজারী বিহীন জমি (শতাংশ) </td>
                        <td>জোত নং</td>  
                        <!--দাগ -->

                        <!--বিক্রিত -->
                        <th>প্লট আইডি নং</th>
                        <th>ব্লক নাম্বার</th>
                        <th>রোড নাম্বার</th>
                        <th>প্লট নাম্বার</th>
                        <th>ক্রেতার নাম</th>
                        <th>বিক্রিত ভূমি </th>
                        <th>মোট  খরচ </th>
                        <!--বিক্রিত -->
                    </tr>
                </thead> 
                <tbody>
                 <?php 
                   $total_sell_land = 0;
                   $total_total_cost = 0;

                  foreach($allreports as $all){
                  $total_sell_land += $all->sell_land;
                  $total_total_cost += $all->total_cost;
                  ?>               
                <tr class="gradeX">
                    <td>{{ $all-> saf_kobola_number}}</td>
                    <td>{{ $all-> data_or_datrir_name}}</td>
                    <td>{{ $all-> name_of_media}}</td>
                    <td>{{ $all-> dolil_number}}</td>
                    <td>{{ $all-> date}}</td>
                    <td>{{ $all-> moujaname}}</td>
                    <td>{{ $all-> sadag_no}}</td>
                    <td>{{ $all-> rs_dag_no}}</td>
                    <td>{{ $all-> rs_dag_a_poriman}}</td>
                    <td>{{ $all-> kas_jomi}}</td>
                    <td>{{ $all-> odigrohan_krrito_jomi}}</td>
                    <td>{{ $all-> rs_dagay_total_jomi}}</td>
                    <td>{{ $all-> total_purchased_land}}</td>
                    <td>{{ $all-> remaining_land}}</td> 
                    <td>{{ $all-> smonnoy_extra_purchased_land}}</td>
                    <td>{{ $all-> namjari_krito_land}}</td>
                    <td>{{ $all-> namjari_bihin_land}}</td>
                    <td>{{ $all-> jooth_number}}</td>

                    <td>{{ $all-> plot_id_number}}</td>
                    <td>{{ $all-> block_name}}</td>
                    <td>{{ $all-> road_number}}</td>
                    <td>{{ $all-> plot_number}}</td>
                    <td>{{ $all-> buyer_name}}</td>
                    <td>{{ $all-> sell_land}}</td>
                    <td>{{ $all-> total_cost}}</td>
                </tr>
                  <?php 
                  }
                  ?>
                </tbody>
                <tfoot>
                    <tr style="color: black; font-weight: bold;">
                      <td colspan="23">Total</td>
                      <td>{{$total_sell_land}}</td>
                      <td>{{$total_total_cost}}</td>
                    </tr>
                </tfoot>
            </table>
          </div>
        </div>       
      </div>
    </div>
</div>
@endsection