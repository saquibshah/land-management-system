<?php
// My common functions
function numberFormat($amount,$coma=null)
{
	if($amount==0){
	   return '-';
	}
	else{
	    return number_format($amount,2);
	}
}
function dateFormat($date,$time=null)
{
	if($time){
	    return date('d/m/Y h:i A',strtotime($date));
	}else{
		return date('d/m/Y',strtotime($date));
	}
}
function dumpvar($array)
{
	echo '<pre>';
	print_r($array);
}
?>