<?php 
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Illuminate\Http\Request;
use Input;


class DagrejisterController extends Controller {

	public function index()
	{
		$result=DB::table('dag_register')
		->join('mouja', 'mouja.id', '=', 'dag_register.mouja_id')
		//->join('grohita', 'grohita.id', '=', 'dag_register.grohitar_name')
		->select('dag_register.*','mouja.nameOfMouja as moujaname')->get();	
		//print_r($result);	
		return view('dagrejister.dagrejister')->with('dagrejisters',$result);
	}
	
	public function create(Request $request)
	{
		$post=$request->all();
		$data= array(
		'saf_kobola_number'			   =>$post['saf_kobola_number'], 
		'data_or_datrir_name'     	   =>$post['data_or_datrir_name'], 
		'grohitar_name' 			   =>$post['grohitar_name'], 
		'name_of_media' 			   =>$post['name_of_media'], 
		'dolil_number' 				   =>$post['dolil_number'], 
		'date'         				   =>$post['date'], 
		'mouja_id'     				   =>$post['mouja'],
		'rs_dag_no' 				   =>$post['rs_dag_no'], 
		'sadag_no'     				   =>$post['sadag_no'],
		'rs_dag_a_poriman' 			   =>$post['rs_dag_a_poriman'], 
		'kas_jomi'                     =>$post['kas_jomi'], 
		'odigrohan_krrito_jomi'        =>$post['odigrohan_krrito_jomi'], 
		'rs_dagay_total_jomi'          =>$post['rs_dagay_total_jomi'], 
		'total_purchased_land'         =>$post['total_purchased_land'], 
		'baynakrito_jomi'              =>$post['baynakrito_jomi'], 
		'remaining_land'               =>$post['remaining_land'], 
		'smonnoy_extra_purchased_land' =>$post['smonnoy_extra_purchased_land'],
		'namjari_krito_land'           =>$post['namjari_krito_land'],
		'namjari_bihin_land'           =>$post['namjari_bihin_land'], 
		'jooth_number'                 =>$post['jooth_number'], 
		'keis_no'                      =>$post['keis_no'], 
		'khotiyan_no'                  =>$post['khotiyan_no']
		);
		//var_dump($data); die();
		$mouja_update=DB::table('mouja_dag')
						->where('rs_dag_no','=' ,$post['rs_dag_no'])
            			->update(['rs_dag_remaing_land' => $post['remaining_land']]);
        //var_dump($mouja_update); die();
		$i=DB::table('dag_register')->insert($data);
		
		 return redirect('/dagrejister/create');	
	}


	public function edit($id){
    	$row=DB::table('dag_register AS A')
				->select('A.*', DB::raw("(SUM(B.total_purchased_land)-A.total_purchased_land) as totalRegister"))
				->leftJoin('dag_register AS B', 'A.rs_dag_no', '=', 'B.rs_dag_no')
				->where('A.id',$id)
				->first();
				
		return view("dagrejister.edit")->with("row",$row);
	}
	public function update(Request $request)
	{
		$post=$request->all();
		$data=array(
		'saf_kobola_number'		       =>$post['saf_kobola_number'], 
		'data_or_datrir_name'     	   =>$post['data_or_datrir_name'], 
		'grohitar_name' 			   =>$post['grohitar_name'], 
		'name_of_media' 			   =>$post['name_of_media'], 
		'dolil_number' 				   =>$post['dolil_number'], 
		'date'         				   =>$post['date'], 
		'mouja_id'     				   =>$post['mouja'],
		'rs_dag_no' 				   =>$post['rs_dag_no'], 
		'sadag_no'     				   =>$post['sadag_no'],
		'rs_dag_a_poriman' 			   =>$post['rs_dag_a_poriman'], 
		'kas_jomi'                     =>$post['kas_jomi'], 
		'odigrohan_krrito_jomi'        =>$post['odigrohan_krrito_jomi'], 
		'rs_dagay_total_jomi'          =>$post['rs_dagay_total_jomi'], 
		'total_purchased_land'         =>$post['total_purchased_land'], 
		'baynakrito_jomi'              =>$post['baynakrito_jomi'], 
		'remaining_land'               =>$post['remaining_land'], 
		'smonnoy_extra_purchased_land' =>$post['smonnoy_extra_purchased_land'],
		'namjari_krito_land'           =>$post['namjari_krito_land'],
		'namjari_bihin_land'           =>$post['namjari_bihin_land'], 
		'jooth_number'                 =>$post['jooth_number'],
		'keis_no'                      =>$post['keis_no'], 
		'khotiyan_no'                  =>$post['khotiyan_no']

		 );
		   
			$i=DB::table('dag_register')->where('id',$post["txtId"])->update($data);
			return redirect('dagrejister/');	
		
	
	}

	public function delete($id){
		
		DB::table('dag_register')->where('id',$id)->delete();
			
		return redirect()->back();	
		
	}

	

	public function dag_report(){
	    $result=DB::table('dag_register AS A')
		->join('mouja AS B', 'B.id', '=', 'A.mouja_id')
		->join('mouja_dag AS C', function($join) {
			$join->on('A.rs_dag_no', '=', 'C.rs_dag_no');
			$join->on('A.mouja_id', '=', 'C.mouja_id');
		}) 
		->select('A.*','B.*','C.*')
		->orderBy('A.rs_dag_no', 'ASC')
		->orderBy(DB::raw('CAST(A.saf_kobola_number AS SIGNED)'), 'ASC')
		->get();	
		return view('dagrejister.dagregister_report')->with('dagrejisters',$result);
	}
	
	public function details($id){
    	$result=DB::table('dag_register')
    			->join('mouja', 'mouja.id', '=', 'dag_register.mouja_id')
				->select('dag_register.*','mouja.nameOfMouja as moujaname')
				->where('dag_register.id',$id)
				->get();	
		return view('dagrejister.details')->with('dagdetails',$result);
	}

	public function excel()
	{
		$users = DB::table('dag_register')
		->join('mouja', 'mouja.id', '=', 'dag_register.mouja_id')
		//->join('mouja_dag', 'dag_register.rs_dag_no', '=', 'mouja_dag.rs_dag_no')
		->select('rs_dag_no', 'saf_kobola_number', 'data_or_datrir_name', 'name_of_media', 'dolil_number', 'date', 'nameOfMouja', 'sadag_no', 'rs_dag_a_poriman', 'kas_jomi', 'odigrohan_krrito_jomi', 'rs_dagay_total_jomi','rs_dag_remaing_land', 'total_purchased_land', 'remaining_land', 'smonnoy_extra_purchased_land', 'namjari_krito_land', 'namjari_bihin_land', 'jooth_number','keis_no','khotiyan_no')
		 ->orderBy('dag_register.rs_dag_no', 'ASC')
		->get();
		$users = json_decode( json_encode($users), true);
		
		Excel::create('dag_register', function($excel) use($users) {
	    $excel->sheet('Data', function($sheet) use ($users) {
	        $sheet->appendRow(['আর এস দাগ নং','সাফ কবলা ফাইল নং','দাতা/দাত্রীর নাম','মিডিয়ার নাম','দলিল নং','তারিখ','মৌজা','এস এ দাগ নং','আর এস দাগে মোট পরিমাণ (শতাংশ)','খাস জমি (শতাংশ)','অধিগ্রহণ কৃত জমি(শতাংশ)','R/S দাগে মোট ক্রয়যোগ্য জমি  (শতাংশ)','R/S দাগে মোট অবশিষ্ট ক্রয়যোগ্য জমি  (শতাংশ)','ক্রয়কৃত জমির পরিমাণ (শতাংশ)','অবশিষ্ট জমির পরিমাণ (শতাংশ)','সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি ((শতাংশ)','নামজারীকৃত জমি (শতাংশ)','নামজারী বিহীন জমি (শতাংশ)','জোত নাম্বার','নামজারী কেইস নাম্বার','নামজারী খতিয়ান নাম্বার ']);
	        foreach ($users as $source) {
	            $sheet->appendRow((array)$source);
	        }
	      });
	    })->export('xls');
	//========================================================================//
	}

	public function exp_excel()
	{
		Excel::create('New file', function($excel) {
        $excel->sheet('New sheet', function($sheet) {
        $result = DB::table('dag_register')
		          ->join('mouja', 'mouja.id', '=', 'dag_register.mouja_id')
		          ->join('mouja_dag', 'dag_register.rs_dag_no', '=', 'mouja_dag.rs_dag_no')
				  ->select('*')
		          ->orderBy('dag_register.rs_dag_no', 'ASC')
		          ->get();
        $sheet->loadView('dagrejister.dagregister_report')->with('dagrejisters',$result);
        });
	})->export('xls');
		
	}

	public function get_ajax()
	{
		//total_purchased_land
		$rs_dag_no = Input::get('rs_dag_no');
		$mouja = Input::get('mouja');
		$subrs_dag = DB::table('mouja_dag AS A')
				   ->select('A.id', 'A.sa_dag_no', 'A.rs_dag_no', 'A.rs_dag_total_amount', 'A.mouja_id', 'A.kas_jomi', 'A.odigrohon_jomi', DB::raw("SUM(B.total_purchased_land) as totalRegister"))
				   ->leftJoin('dag_register AS B', function($join) {
				  	$join->on('A.rs_dag_no', '=', 'B.rs_dag_no');
				  	$join->on('A.mouja_id', '=', 'B.mouja_id');
				   }) 
		           ->where('A.rs_dag_no','=',$rs_dag_no)
		           ->where('A.mouja_id','=',$mouja)
		           ->get();
		return $subrs_dag;
		
	}
}