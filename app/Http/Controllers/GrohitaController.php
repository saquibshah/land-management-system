<?php 
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Illuminate\Http\Request;



class GrohitaController extends Controller {

	public function index()
	{
		$result=DB::table('grohita')->get();	
		//print_r($result);	
		return view('grohita.grohita')->with('grohitas',$result);
	}
	
	public function create(Request $request)
	{
		$post=$request->all();
		$data= array(
		'grohitar_name' =>$post['grohitar_name'] 
		);
		//var_dump($data); die();
		$i=DB::table('grohita')->insert($data);
		
		 return redirect('/grohita/create');	
		
	}
	public function update(Request $request)
	{
		$post=$request->all();
		$data= array(
		'grohitar_name' =>$post['grohitar_name'] 
		);
		//var_dump($data); die();
		$i=DB::table('grohita')->where('id',$post["txtId"])->update($data);
		
		 return redirect('grohita/');	
		
	}

	public function delete($id){
		
		DB::table('grohita')->where('id',$id)->delete();
			
		return redirect()->back();	
		
	}
}