<?php 
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Excel;

use Illuminate\Http\Request;



class MoujaaddController extends Controller {

	public function index()
	{
		$result=DB::table('mouja')->get();	
		//print_r($result);	
		return view('moujaadd.moujaadd')->with('moujas',$result);
	}
	
	public function create(Request $request)
	{
		$post=$request->all();
		$data= array(
		'nameOfMouja' =>$post['nameOfMouja'] 
		);
		//var_dump($data); die();
		$i=DB::table('mouja')->insert($data);
	
		 return redirect('/moujaadd');	
			
	}

	public function update(Request $request)
	{
		$post=$request->all();
		$data= array(
		'nameOfMouja' =>$post['nameOfMouja'] 
		);
		//var_dump($data); die();
		$i=DB::table('mouja')->where('id',$post["txtId"])->update($data);
	
		 return redirect('moujaadd/');	
			
	}

	public function delete($id){
		
		DB::table('mouja')->where('id',$id)->delete();
			
		return redirect()->back();	
		
	}

	public function excel()
	{
		$users = DB::table('mouja')->select('*')->get();
		$users = json_decode( json_encode($users), true);
		/*Excel::create('mouja', function($excel) use($users) {
			$excel->sheet('Sheet 1', function($sheet) use($users) {
		    $sheet->fromArray(array(
	            array('data1', 'data2'),
	            array($users)));
		    });
		})->export('xls');*/
	//=================================================================//
		Excel::create('mouja', function($excel) use($users) {
	    $excel->sheet('Data', function($sheet) use ($users) {
	        $sheet->appendRow(['আইডি নং','মৌজার নাম']);
	        foreach ($users as $source) {
	            $sheet->appendRow((array)$source);
	        }
	      });
	    })->export('xls');
	//========================================================================//
	}
}