<?php 
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Excel;
use Illuminate\Http\Request;



class PurchaseregisterController extends Controller {

	public function index()
	{
		//$result=DB::table('purchase_register')->get();
		$result= DB::table('purchase_register')
               ->join('dag_register', 'purchase_register.saf_kobola_number', '=', 'dag_register.saf_kobola_number')
               ->join('mouja', 'dag_register.mouja_id', '=', 'mouja.id')
               ->select('*','purchase_register.id as purchaseId')
               ->orderBy('purchase_register.saf_kobola_number', 'ASC')
               ->get(); 

		//print_r($result);	
		return view('purchaseregister.purchaseregister')->with('purchaseregisters',$result);
	}
	
	public function create(Request $request)
	{
		$post=$request->all();
		$data= array(
		'saf_kobola_number'			       =>$post['saf_kobola_number'], 
		//'rs_dag_no'			               =>$post['rs_dag_no'], 
		//'korida_onso'			           =>$post['korida_onso'], 
		'bina_budget_number'			   =>$post['bina_budget_number'], 
		'bina_total_budget_money'     	   =>$post['bina_total_budget_money'], 
		'bina_datak_prodan' 			   =>$post['bina_datak_prodan'], 
		'bina_registration_cost' 		   =>$post['bina_registration_cost'], 
		'bina_meadia_commision' 		   =>$post['bina_meadia_commision'], 
		'bina_anosongik_cost'         	   =>$post['bina_anosongik_cost'], 
		'bina_total_cost'     			   =>$post['bina_total_cost'],
		'bina_returned_money' 			   =>$post['bina_returned_money'], 
		'budget_number'     			   =>$post['budget_number'],
		'total_budget_money' 			   =>$post['total_budget_money'], 
		'toatal_price_of_purchased_land'   =>$post['toatal_price_of_purchased_land'], 
		'mentioned_price_in_dolil'         =>$post['mentioned_price_in_dolil'], 
		'pe_order_mullo'                   =>$post['pe_order_mullo'], 
		'registration_cost'                =>$post['registration_cost'], 
		'meadia_commision'                 =>$post['meadia_commision'], 
		'anosongik_cost'                   =>$post['anosongik_cost'], 
		'total_cost'                       =>$post['total_cost'],
		'returned_money'          		   =>$post['returned_money'],
		'sorbomot'          			   =>$post['sorbomot'], 
		'montobbo'                		   =>$post['montobbo'] 
		);
		//var_dump($data); die();
		$i=DB::table('purchase_register')->insert($data);
		
		 return redirect('/purchaseregister/create');	
	
	}
	public function update(Request $request)
	{
		$post=$request->all();
		$data= array(
		'saf_kobola_number'			       =>$post['saf_kobola_number'], 
		'bina_budget_number'			   =>$post['bina_budget_number'], 
		'bina_total_budget_money'     	   =>$post['bina_total_budget_money'], 
		'bina_datak_prodan' 			   =>$post['bina_datak_prodan'], 
		'bina_registration_cost' 		   =>$post['bina_registration_cost'], 
		'bina_meadia_commision' 		   =>$post['bina_meadia_commision'], 
		'bina_anosongik_cost'         	   =>$post['bina_anosongik_cost'], 
		'bina_total_cost'     			   =>$post['bina_total_cost'],
		'bina_returned_money' 			   =>$post['bina_returned_money'], 
		'budget_number'     			   =>$post['budget_number'],
		'total_budget_money' 			   =>$post['total_budget_money'], 
		'toatal_price_of_purchased_land'   =>$post['toatal_price_of_purchased_land'], 
		'mentioned_price_in_dolil'         =>$post['mentioned_price_in_dolil'], 
		'pe_order_mullo'                   =>$post['pe_order_mullo'], 
		'registration_cost'                =>$post['registration_cost'], 
		'meadia_commision'                 =>$post['meadia_commision'], 
		'anosongik_cost'                   =>$post['anosongik_cost'], 
		'total_cost'                       =>$post['total_cost'],
		'returned_money'          		   =>$post['returned_money'],
		'sorbomot'          			   =>$post['sorbomot'], 
		'montobbo'                		   =>$post['montobbo'] 
		);
		//var_dump($data); die();
		$i=DB::table('purchase_register')->where('id',$post["txtId"])->update($data);
		
		 return redirect('purchaseregister/');	
	
	}

	public function delete($id){
		
		DB::table('purchase_register')->where('id',$id)->delete();
			
		return redirect()->back();	
		
	}

	public function pur_report(){
		$result = DB::table('purchase_register')
				  ->join('dag_register', 'purchase_register.saf_kobola_number', '=', 'dag_register.saf_kobola_number')
				  ->select('*')
				  ->orderBy(DB::raw('CAST(purchase_register.saf_kobola_number AS SIGNED)'), 'ASC')
		          ->get();

		return view('purchaseregister.purchase_report')->with('purchaseas',$result);

	}

	public function excel()
	{
		$users = DB::table('purchase_register')
		 		->join('purchase_details', 'purchase_register.saf_kobola_number', '=', 'purchase_details.id')
			    ->select('saf_kobola_number', 'rs_dag_no', 'bina_budget_number','bina_total_budget_money', 'bina_datak_prodan', 'bina_registration_cost', 'bina_meadia_commision', 'bina_anosongik_cost','bina_total_cost', 'bina_returned_money', 'budget_number','total_budget_money', 'mentioned_price_in_dolil','registration_cost', 'meadia_commision', 'anosongik_cost', 'total_cost', 'returned_money','sorbomot', 'montobbo')
			    ->orderBy('purchase_register.saf_kobola_number', 'ASC')
	            ->get();
		$users = json_decode( json_encode($users), true);
		
		Excel::create('purchase_register', function($excel) use($users) {
	    $excel->sheet('Data', function($sheet) use ($users) {
	        $sheet->appendRow(['সাফ কবলা ফাইল নং','আর এস দাগ নং','বায়না বাজেট নং','বায়না বাজেটকৃত মোট টাকা','দাতাকে প্রদানকৃত টাকা','বায়না রেজিস্ট্রেশন খরচ','বায়না মিডিয়া কমিশন','বায়না আনুষঙ্গিক খরচ','বায়না সর্বমোট খরচ','বায়না ফেরতকৃত টাকা','সাফ কবলা বাজেট নং','বাজেটকৃত মোট টাকা','দলিলে প্রদর্শিত মূল্য','রেজিস্ট্রেশন খরচ','মিডিয়া কমিশন','আনুষঙ্গিক খরচ','সাফ কবলায় মোট খরচ','ফেরতকৃত টাকা (যদি থাকে)','সর্বমোট খরচ','মন্তব্য']);
	        foreach ($users as $source) {
	            $sheet->appendRow((array)$source);
	        }
	      });
	    })->export('xls');
	//========================================================================//
	}

	public function exp_excel()
	{
		Excel::create('New file', function($excel) {
        $excel->sheet('New sheet', function($sheet) {
        $result = DB::table('purchase_register')
				  ->join('dag_register', 'purchase_register.saf_kobola_number', '=', 'dag_register.saf_kobola_number')
				  ->select('*')
				  ->orderBy('purchase_register.saf_kobola_number', 'ASC')
		          ->get();
        $sheet->loadView('purchaseregister.purchase_report')->with('purchaseas',$result);
        });
	    })->export('xls');
		
	}
}