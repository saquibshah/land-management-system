<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Illuminate\Http\Request;

class TotalpurchaseController extends Controller
{
	public function index()
	{
		$query_rep= DB::table('dag_register')
		->join('mouja', 'dag_register.mouja_id', '=', 'mouja.id')
		->join('mouja_dag', function($join)
		{
			$join->on('dag_register.rs_dag_no', '=', 'mouja_dag.rs_dag_no');
			$join->on('dag_register.mouja_id', '=', 'mouja_dag.mouja_id');
		}) 
		->orderBy(DB::raw('CAST(dag_register.saf_kobola_number AS SIGNED)'), 'ASC')     
		->select('dag_register.*','mouja.nameOfMouja as moujaname');

		$nameOfMouja = Input::get('mouja');
		if(!empty($nameOfMouja)) {
          $query_rep->where('dag_register.mouja_id', $nameOfMouja);
	    }

	    $fromDate = Input::get('fromDate'); // y-m-d
		$toDate = Input::get('toDate');
	    if(!empty($fromDate) && !empty($toDate)) {
            $query_rep->whereBetween("dag_register.date", [$fromDate, $toDate]);
        }
		$result=$query_rep->get();	
		//dumpvar($result);exit;	
	
		return view('report.purchase_final')->with('totalpurchases',$result);
	}
}
