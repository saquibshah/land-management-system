<?php 
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Input;

class LandController extends Controller {

	public function index()
	{
		 
        $result=DB::table('land_details')
			   ->get();		
		//print_r($result);	
		return view('land.land')->with('lands',$result);
	}
	
	public function create(Request $request)
	{
		$dataArray = [];

		$post=$request->all();
		$data['block_name'] = $post['block_name'];
		foreach ($post['road_number'] as $key => $road) {
			$data['road_number'] = $road;
			$data['face'] = $post['face'][$key];
			foreach ($post['plot_number'][$key] as $k => $plot) {
				$data['plot_number'] = $plot;
				$data['plot_size'] = $post['plot_size'][$key][$k];

				$dataArray[] = $data;
			}
		}
		DB::table('land_details')->insert($dataArray);	
		return redirect('/land');
	}

	public function update(Request $request)
	{
		$post=$request->all();
		$data=array(
			'block_name'  =>$post['block_name'],
			'road_number' =>$post['road_number'],
			'face'        =>$post['face'],
			'plot_number' =>$post['plot_number'],
			'plot_size'   =>$post['plot_size']
		);
		
		//var_dump($data); die();
		$i=DB::table('land_details')->where('id',$post["txtId"])->update($data);
		
		 return redirect('land/');	
	
	}

	public function edit($id){

	    $row = DB::table('land_details')
		     ->where('id',$id)
		     ->first();
		  
	    return view("land.edit")->with("row",$row);
		
	}


	public function delete($id){
		
		DB::table('land_details')->where('id',$id)->delete();
			
		return redirect()->back();	
		
	}

	public function ajaxGetRoad(){

		$block_name = Input::get('block_name');
        $subcategories = DB::table('land_details')
        				->select('road_number')
        				->where('block_name','=',$block_name)
        				->groupBy('road_number')
        				->get();
      //dd($subcategories);
       return $subcategories;

	}

	public function ajaxGetPlot(){

		$block_name = Input::get('block_name');
		$road_number = Input::get('road_number');
        $subcategories = DB::table('land_details')
        				->select('face','plot_number')
        				->where('block_name','=',$block_name)
        				->where('road_number','=',urldecode($road_number))
        				->groupBy('plot_number')
        				->get();
      //dd($subcategories);
       return $subcategories;

	}

	public function ajaxGetSize(){
		$block_name = Input::get('block_name');
		$road_number = Input::get('road_number');
		$plot_number = Input::get('plot_number');
        $subcategories = DB::table('land_details')
        				->select('id','plot_size')
        				->where('block_name','=',$block_name)
        				->where('road_number','=',$road_number)
        				->where('plot_number','=',$plot_number)
        				->get();
		//dd($subcategories);
        return $subcategories;

	}
}