<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Illuminate\Http\Request;

class SellregisterController extends Controller {

	public function index()
	{
		$result=DB::table('sell_register')
		      ->join('mouja', 'mouja.id', '=', 'sell_register.mouja_id')
		      ->LeftJoin('land_details', 'land_details.id', '=', 'sell_register.land_id')
		      ->select('sell_register.*','mouja.nameOfMouja as moujaname','land_details.block_name','land_details.road_number','land_details.face','land_details.plot_number','land_details.plot_size')
		      ->get();	
		
		return view('sellregister.sellregister')->with('sellregisters',$result);
	}
	
	public function create(Request $request)
	{
		$post=$request->all();
		$data= array(
		'plot_sell_file_number'    =>$post['plot_sell_file_number'], 
		'plot_id_number'     	   =>$post['plot_id_number'], 
		'land_id' 			       =>$post['land_id'], 
		'buyer_name' 		       =>$post['buyer_name'], 
		'media_name'     	       =>$post['media_name'],
		'mouja_id'     	           =>$post['mouja_id'],
		'rs_dag_no'     	       =>$post['rs_dag_no'],
		'sell_land'     	       =>$post['sell_land'],
		'saf_kobola_number'        =>implode(', ', $post['saf_kobola_number']),
		'sell_land_price'     	   =>$post['sell_land_price'],
		'sell_land_dev_cost'       =>$post['sell_land_dev_cost'],
		'registry_cost'            =>$post['registry_cost'],
		'others_cost'              =>$post['others_cost'],
		'total_cost'               =>$post['total_cost'],
		'dolil_no'                 =>$post['dolil_no'],
		'sell_date'                =>$post['sell_date'],
		'comment'                  =>$post['comment']
		
		);
		//var_dump($data); die();
		$i=DB::table('sell_register')->insert($data);
		
		 return redirect('/sellregister/create');	
	
	}

	public function update(Request $request)
	{
		$post=$request->all();
		$data= array(
		'plot_sell_file_number'    =>$post['plot_sell_file_number'], 
		'plot_id_number'     	   =>$post['plot_id_number'], 
		'land_id' 			       =>$post['land_id'], 
		'buyer_name' 		       =>$post['buyer_name'], 
		'media_name'     	       =>$post['media_name'],
		'mouja_id'     	           =>$post['mouja_id'],
		'rs_dag_no'     	       =>$post['rs_dag_no'],
		'sell_land'     	       =>$post['sell_land'],
		'saf_kobola_number'        =>implode(', ', $post['saf_kobola_number']),
		'sell_land_price'     	   =>$post['sell_land_price'],
		'sell_land_dev_cost'       =>$post['sell_land_dev_cost'],
		'registry_cost'            =>$post['registry_cost'],
		'others_cost'              =>$post['others_cost'],
		'total_cost'               =>$post['total_cost'],
		'dolil_no'                 =>$post['dolil_no'],
		'sell_date'                =>$post['sell_date'],
		'comment'                  =>$post['comment']
		
		
		);
		//var_dump($data); die();
		$i=DB::table('sell_register')->where('id',$post["txtId"])->update($data);
		
		 return redirect('sellregister/');	
	
	}

	public function edit($id){

	    $row = DB::table('sell_register AS A')
		    ->where('A.id',$id)
		    ->select('A.*', DB::raw("SUM(B.total_purchased_land) as totalRegister"), DB::raw("(C.totalSell-A.sell_land) as totalSell"),'land_details.block_name','land_details.road_number','land_details.face','land_details.plot_number','land_details.plot_size')
		    ->leftJoin('dag_register AS B', function($join)
			{
				$join->on('A.rs_dag_no', '=', 'B.rs_dag_no');
				$join->on('A.mouja_id', '=', 'B.mouja_id');
			}) 
		    ->leftJoin(DB::raw("(SELECT mouja_id, rs_dag_no, SUM(sell_land) as totalSell FROM sell_register GROUP BY mouja_id, rs_dag_no) AS C"), function($join)
				{
					$join->on('A.rs_dag_no', '=', 'C.rs_dag_no');
					$join->on('A.mouja_id', '=', 'C.mouja_id');
				}) 
		    ->LeftJoin('land_details', 'land_details.id', '=', 'A.land_id')
		    ->first();
			
	    return view("sellregister.edit")->with("row",$row);
		
	}


	public function delete($id){
		
		DB::table('sell_register')->where('id',$id)->delete();
			
		return redirect()->back();	
		
	}

	public function getRsDetails(){
		$mouja_id = Input::get('mouja_id');
		$rs_dag_no = Input::get('rs_dag_no');

		$row = DB::table('dag_register AS A')
				->select(DB::raw("SUM(A.total_purchased_land) as totalRegister"), 'B.totalSell')
				->leftJoin(DB::raw("(SELECT mouja_id, rs_dag_no, SUM(sell_land) as totalSell FROM sell_register GROUP BY mouja_id, rs_dag_no) AS B"), function($join)
				{
					$join->on('A.rs_dag_no', '=', 'B.rs_dag_no');
					$join->on('A.mouja_id', '=', 'B.mouja_id');
				}) 
				->where('A.mouja_id',$mouja_id)
				->where('A.rs_dag_no',$rs_dag_no)
				->first();

		$row2 = DB::table('dag_register AS A')
				->select('A.saf_kobola_number')
				->where('A.mouja_id',$mouja_id)
				->where('A.rs_dag_no',$rs_dag_no)
				->groupBy('A.saf_kobola_number')
				->get();

		$data['totalRegister'] = $row->totalRegister;
		$data['totalSell'] = $row->totalSell;
		$data['allSafkobola'] = $row2;
		return json_encode($data);
	}

	public function sellreport(){
		$result = DB::table('sell_register AS A')
		         ->select('A.*','mouja.nameOfMouja as moujaname',DB::raw("SUM(B.total_purchased_land) as totalRegister"),'land_details.block_name','land_details.road_number','land_details.face','land_details.plot_number','land_details.plot_size')//, 'C.totalSell'
				 ->join('mouja', 'mouja.id', '=', 'A.mouja_id')
				 ->LeftJoin('land_details', 'land_details.id', '=', 'A.land_id')
				
		        
		         ->leftJoin('dag_register AS B', function($join)
					{
						$join->on('A.rs_dag_no', '=', 'B.rs_dag_no');
						$join->on('A.mouja_id', '=', 'B.mouja_id');
					}) 
				 /*->leftJoin(DB::raw("(SELECT id, mouja_id, rs_dag_no, SUM(sell_land) as totalSell FROM sell_register GROUP BY mouja_id, rs_dag_no) AS C"), function($join)
				 {
					$join->on('A.rs_dag_no', '=', 'C.rs_dag_no');
					$join->on('A.mouja_id', '=', 'C.mouja_id');
					$join->on('C.id', '<', 'A.id');
				 })*/ 
				 ->groupBy('A.id')
				 ->orderBy(DB::raw('CAST(A.plot_sell_file_number AS SIGNED)'), 'ASC')
		         ->get();	
		

		return view('sellregister.sellreport')->with('sellreports',$result);

	}
	
}