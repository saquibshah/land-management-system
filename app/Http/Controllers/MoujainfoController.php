<?php 
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Illuminate\Http\Request;



class MoujainfoController extends Controller {

	public function index()
	{
		 
        $result=DB::table('mouja')
		->join('mouja_dag', 'mouja.id', '=', 'mouja_dag.mouja_id')
		->select('mouja_dag.*','mouja.nameOfMouja as moujaname')->get();		
		//print_r($result);	
		return view('moujainfo.moujainfo')->with('moujainfos',$result);
	}
	
	public function create(Request $request)
	{
		$post=$request->all();
		$remaining_land = $post['rs_dag_total_amount'] - ( $post['kas_jomi'] + $post['odigrohon_jomi'] );
		$data= array(
		'mouja_id' =>$post['mouja_id'],
		'sa_dag_no' =>$post['sa_dag_no'], 
		'rs_dag_no' =>$post['rs_dag_no'], 
		'rs_dag_total_amount' =>$post['rs_dag_total_amount'], 
		'sorojomine_jomi' =>$post['sorojomine_jomi'], 
		'kas_jomi' =>$post['kas_jomi'], 
		'odigrohon_jomi' =>$post['odigrohon_jomi'],
		'rs_dag_remaing_land' =>$remaining_land 
		);
		//var_dump($data); die();
		$i=DB::table('mouja_dag')->insert($data);
		
		 return redirect('/moujainfo');	
	
	}

	public function update(Request $request)
	{
		$post=$request->all();
		$remaining_land = $post['rs_dag_total_amount'] - ( $post['kas_jomi'] + $post['odigrohon_jomi'] );
		$data= array(
		'mouja_id' =>$post['mouja_id'],
		'sa_dag_no' =>$post['sa_dag_no'], 
		'rs_dag_no' =>$post['rs_dag_no'], 
		'rs_dag_total_amount' =>$post['rs_dag_total_amount'],
		'sorojomine_jomi' =>$post['sorojomine_jomi'], 
		'kas_jomi' =>$post['kas_jomi'], 
		'odigrohon_jomi' =>$post['odigrohon_jomi'], 
		'rs_dag_remaing_land' =>$remaining_land 
		);
		//var_dump($data); die();
		$i=DB::table('mouja_dag')->where('id',$post["txtId"])->update($data);
		
		 return redirect('moujainfo/');	
	
	}

	public function delete($id){
		
		DB::table('mouja_dag')->where('id',$id)->delete();
			
		return redirect()->back();	
		
	}
}