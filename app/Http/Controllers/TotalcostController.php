<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Role;
use Illuminate\Support\Facades\Input;
use DB;

class TotalcostController extends Controller {

	public function index()
	{
		$sql = DB::table('dag_register')
 			->join('purchase_register', 'dag_register.saf_kobola_number', '=', 'purchase_register.saf_kobola_number')
 			->orderBy('dag_register.saf_kobola_number','ASC')
 			->select('purchase_register.*','dag_register.*');

		$nameOfMouja = Input::get('mouja');
		

		if(!empty($nameOfMouja)) {
          $sql->where('dag_register.mouja_id', $nameOfMouja);
	    }

	    $fromDate = Input::get('fromDate'); // y-m-d
		$toDate = Input::get('toDate');

		if(!empty($fromDate) && !empty($toDate)) {
            $sql->whereBetween("dag_register.date", [$fromDate, $toDate]);
        }

		$result = $sql->get();
		return view('report.totalcost')->with('costs',$result);
	}	
}