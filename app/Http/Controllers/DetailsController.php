<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Role;
use Illuminate\Support\Facades\Input;
use DB;

class DetailsController extends Controller {

	public function index()
	{
		$result=DB::table('mouja')
		->join('dag_register', 'mouja.id', '=', 'dag_register.mouja_id')
		->select('dag_register.*','mouja.nameOfMouja as moujaname')->get();	
		//print_r($result);	
		return view('details.details')->with('detailss',$result);
	}

	public function search_report()
	{
		
		return view('details.details_search');	
		
	}

	public function final_report(){

		$mouja_id = Input::get('mouja');
		$rs_dag = Input::get('rs_dag');
		$saf_kobola = Input::get('saf_kobola');

		/*$query_rep = DB::table('purchase_register')
                   ->leftJoin('dag_register', 'purchase_register.saf_kobola_number', '=', 'dag_register.saf_kobola_number')
                   ->leftJoin('mouja', 'mouja.id', '=', 'dag_register.mouja_id')
		           ->select('dag_register.*','mouja.nameOfMouja as moujaname');*/

       $query_rep = DB::table('mouja')
			       ->join('mouja_dag', 'mouja.id', '=', 'mouja_dag.mouja_id')
			       /*->leftJoin('dag_register', 'mouja_dag.rs_dag_no', '=', 'dag_register.rs_dag_no')*/
			       //->leftJoin('purchase_register', 'dag_register.saf_kobola_number', '=', 'purchase_register.saf_kobola_number')
			       ->leftJoin('dag_register', function($join)
					{
						$join->on('dag_register.rs_dag_no', '=', 'mouja_dag.rs_dag_no');
						$join->on('dag_register.mouja_id', '=', 'mouja_dag.mouja_id');
					}) 
			       ->select('dag_register.*', 'mouja.*', 'mouja_dag.*')
			       ->orderBy(DB::raw('CAST(dag_register.saf_kobola_number AS SIGNED)'), 'ASC');
		           

		if(!empty($mouja_id)) {
          $query_rep->where('mouja_dag.mouja_id', $mouja_id);
	    }
	    if(!empty($rs_dag)) {
            $query_rep->where('mouja_dag.rs_dag_no', $rs_dag);
        }
        if(!empty($saf_kobola)) {
            $query_rep->where('dag_register.saf_kobola_number', $saf_kobola);
        }

		$result = $query_rep->get();
		return view('details.details_final')->with('dfinals',$result);
	}	
}