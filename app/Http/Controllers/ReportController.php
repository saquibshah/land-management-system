<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Role;
use Illuminate\Support\Facades\Input;
use DB;
use Excel;

class ReportController extends Controller {

	public function index()

	{
		$result= DB::table('dag_register')
		   //->join('sell_register', 'dag_register.id', '=', 'sell_register.id')
		   ->join('sell_register', 'dag_register.id', '=', 'sell_register.id')
		   ->LeftJoin('land_details', 'land_details.id', '=', 'sell_register.land_id')
		   ->join('mouja', 'mouja.id', '=', 'sell_register.mouja_id')
		   /*->join('sell_register', 'dag_register.saf_kobola_number', '=', 'sell_register.saf_kobola_number')*/
		   ->select('sell_register.*','dag_register.*', 'mouja.nameOfMouja as moujaname','land_details.block_name','land_details.road_number','land_details.face','land_details.plot_number','land_details.plot_size')
		   //->select('dag_register.*','sell_register.*','mouja.*')
		   ->get(); 
		return view('report.print_all')->with('allreports',$result);
	}

	public function excel()
	{
		$users = DB::table('dag_register')
		->join('mouja', 'mouja.id', '=', 'dag_register.mouja_id')
		//->join('mouja_dag', 'dag_register.rs_dag_no', '=', 'mouja_dag.rs_dag_no')
		->select('rs_dag_no', 'saf_kobola_number', 'data_or_datrir_name', 'grohitar_name', 'name_of_media', 'dolil_number', 'date', 'nameOfMouja', 'sadag_no', 'rs_dag_a_poriman', 'kas_jomi', 'odigrohan_krrito_jomi', 'rs_dagay_total_jomi', 'total_purchased_land', 'baynakrito_jomi', 'remaining_land', 'smonnoy_extra_purchased_land', 'namjari_krito_land', 'namjari_bihin_land', 'jooth_number')
		 ->orderBy('dag_register.rs_dag_no', 'ASC')
		->get();
		$users = json_decode( json_encode($users), true);
		
		Excel::create('dag_register', function($excel) use($users) {
	    $excel->sheet('Data', function($sheet) use ($users) {
	        $sheet->appendRow(['আর এস দাগ নং','সাফ কবলা ফাইল নং','দাতা/দাত্রীর নাম','গ্রহীতার নাম','মিডিয়ার নাম','দলিল নং','তারিখ','মৌজা','এস এ দাগ নং','আর এস দাগে মোট পরিমাণ (শতাংশ)','খাস জমি (শতাংশ)','অধিগ্রহণ কৃত জমি(শতাংশ)','R/S দাগে মোট ক্রয়যোগ্য জমি  (শতাংশ)','ক্রয়কৃত জমির পরিমাণ (শতাংশ)','বায়নাকৃত জমির পরিমাণ (শতাংশ)','অবশিষ্ট জমির পরিমাণ (শতাংশ)','সমন্বয় / অতিরিক্ত ক্রয়কৃত ভূমি ((শতাংশ)','নামজারীকৃত জমি (শতাংশ)','নামজারী বিহীন জমি (শতাংশ)','জোত নাম্বার']);
	        foreach ($users as $source) {
	            $sheet->appendRow((array)$source);
	        }
	      });
	    })->export('xls');
	//========================================================================//
	}
	
	
	
	
}