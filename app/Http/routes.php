<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/wel', function () {
    return view('welcome');
});

Route::auth();

Route::group(['middleware' => ['auth']], function () {



  Route::get('/', 'HomeController@index');
  Route::get('moujaadd','MoujaaddController@index');
  Route::get('moujaadd/create',function(){	
    return view('moujaadd/create');
  });

  Route::get('moujatoexcel','MoujaaddController@excel');

  Route::post('moujaadd/create','MoujaaddController@create');
  Route::get('moujaadd/edit/{id}',function($id){
   $row=App\Moujaadd::find($id);
   return view("moujaadd.edit")->with("row",$row);
    
  });
  Route::post('moujaadd/update','MoujaaddController@update');
  Route::get('moujaadd/delete/{id}','MoujaaddController@delete');


  Route::get('moujainfo','MoujainfoController@index');
  Route::get('moujainfo/create',function(){	
    return view('moujainfo/create');
  });
  Route::post('moujainfo/create','MoujainfoController@create');
  Route::get('moujainfo/edit/{id}',function($id){
   $row=App\Moujainfo::find($id);
   return view("moujainfo.edit")->with("row",$row);
    
  });
  Route::post('moujainfo/update','MoujainfoController@update');
  Route::get('moujainfo/delete/{id}','MoujainfoController@delete');


  //Route::get('/home', 'HomeController@index');
  Route::get('dagrejister','DagrejisterController@index');
  Route::get('dagrejister/create',function(){	
    return view('dagrejister/create');
  });
  Route::post('dagrejister/create','DagrejisterController@create');
  Route::get('dagrejister/edit/{id}','DagrejisterController@edit');

  Route::post('dagrejister/update','DagrejisterController@update');
  Route::get('dagrejister/delete/{id}','DagrejisterController@delete');
  Route::get('dagreport','DagrejisterController@dag_report');
  Route::get('dagrejister/details/{id}','DagrejisterController@details');

  Route::get('dagtoexcel','DagrejisterController@excel');
  Route::get('dagexcel','DagrejisterController@exp_excel');



  Route::get('grohita','GrohitaController@index');
  Route::get('grohita/create',function(){	
    return view('grohita/create');
  });
  Route::post('grohita/create','GrohitaController@create');
  Route::get('grohita/edit/{id}',function($id){
   $row=App\Grohitainfo::find($id);
   return view("grohita.edit")->with("row",$row);
    
  });
  Route::post('grohita/update','GrohitaController@update');
  Route::get('grohita/delete/{id}','GrohitaController@delete');

  Route::get('purchaseregister','PurchaseregisterController@index');
  Route::get('purchaseregister/create',function(){	
    return view('purchaseregister/create');
  });
  Route::post('purchaseregister/create','PurchaseregisterController@create');
  Route::get('purchaseregister/edit/{id}',function($id){
   $row=App\purchaseregister::find($id);
   $saf_kobola = App\Dagregister::where('saf_kobola_number','=',$row->saf_kobola_number)->get();

   return view("purchaseregister.edit")->with("row",$row)->with('saf_kobola',$saf_kobola);
  	
  });
  Route::post('purchaseregister/update','PurchaseregisterController@update');
  Route::get('purchaseregister/delete/{id}','PurchaseregisterController@delete');
  Route::get('purchase','PurchaseregisterController@pur_report');
  Route::get('purtoexcel','PurchaseregisterController@excel');

  Route::get('exp_excel','PurchaseregisterController@exp_excel');

  Route::get('details','DetailsController@index');
  Route::get('search','DetailsController@search_report');

  Route::POST('search/final_report', [
      'as' => 'search/final_report', 'uses' => 'DetailsController@final_report'
  ]);

  Route::post('total','TotalcostController@index');
  Route::get('total','TotalcostController@index');
  Route::get('purchase_report','TotalpurchaseController@index');
  Route::post('purchase_report','TotalpurchaseController@index');

 
  Route::get('sellregister','SellregisterController@index');
  Route::get('sellregister/create',function(){	
    return view('sellregister/create');
  });
  Route::get('sellregister-getrs-details','SellregisterController@getRsDetails');
  Route::post('sellregister/create','SellregisterController@create');
  /*Route::get('sellregister/edit/{id}',function($id){
   $row=App\Sellregister::find($id);
   return view("sellregister.edit")->with("row",$row);
    
  });*/
  Route::get('sellregister/edit/{id}','SellregisterController@edit');
  Route::post('sellregister/update','SellregisterController@update');
  Route::get('sellregister/delete/{id}','SellregisterController@delete');
  Route::get('sellreport','SellregisterController@sellreport');

  Route::get('land','LandController@index');
  Route::get('land/create',function(){  
    return view('land/create');
  });
  Route::post('land/create','LandController@create');
  Route::get('land/edit/{id}','LandController@edit');
  Route::post('land/update','LandController@update');
  Route::get('land/delete/{id}','LandController@delete');


  Route::get('allreport','ReportController@index');
  Route::get('reporttoexcel','ReportController@excel');


  Route::get('/information/create/ajax-state',function()
  {
      $state_id = Input::get('state_id');
      $subcategories = App\Moujainfo::where('mouja_id','=',$state_id)->get();
      //dd($subcategories);
      return $subcategories;
  });

  /*For Sell Register*/
  Route::get('/information/create/get-roadno','LandController@ajaxGetRoad');
  Route::get('/information/create/get-plot','LandController@ajaxGetPlot');
  Route::get('/information/create/get-size','LandController@ajaxGetSize');

  Route::get('/information/create/ajax-rs-dag','DagrejisterController@get_ajax');

  Route::get('/information/create/ajax-saf-kobola',function()
  {
      $saf_kobola_number = Input::get('saf_kobola_number');
      $saf_kobola = App\Dagregister::where('saf_kobola_number','=',$saf_kobola_number)->get();
      //dd($subcategories);
      return $saf_kobola;
  });
  
});


